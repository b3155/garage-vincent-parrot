/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        "candyred": "#d92332",
        "alizarin": "#d94350",
        "candypink" : "#d9777f",
        "raisin": "#262526",
        "smoke": "#f2f2f2"
      },
      fontFamily: {
        "barlow": "Barlow, sans-serif",
        "rajdhani": "'Rajdhani', sans-serif"
      },
      fontSize: {
        "h1": "clamp(30px, 5vw, 66px)",
        "h2": "clamp(25px, 4vw, 46px)",
        "h3": "clamp(18px, 3vw, 36px)"
      }
    },
  },
  plugins: [],
}

