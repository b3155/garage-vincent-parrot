import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import { nodePolyfills } from 'vite-plugin-node-polyfills'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    nodePolyfills(),
    react(),
  ],

  sourcemap: true,

  resolve: {
    mainFields: [],
    alias: {
      process: "process/browser"
    }
  }
})
