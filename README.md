# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

<br>

# API is based on Firebase

For purpose of deployment you must set environment variables

```bash
VITE_FIREBASE_API_KEY=<FIREBASE_API_KEY>
VITE_FIREBASE_AUTH_DOMAIN=<FIREBASE_AUTH_DOMAIN>
VITE_FIREBASE_PROJECT_ID=<FIREBASE_PROJECT_ID>
VITE_FIREBASE_STORAGE_BUCKET=<FIREBASE_STORAGE_BUCKET>
VITE_FIREBASE_MESSAGING_SENDER_ID=<FIREBASE_MESSAGING_SENDER_ID>
VITE_FIREBASE_APP_ID=<FIREBASE_APP_ID>
```

# Application demo

The URL link for demo [app](https://garage-vincent-parrot.vercel.app/)

Admin email : parrotvincent@gmail.com

Admin password : test123

*Note : User email and password shouldn't be shared here, this is only for testing demo app