import React from "react";
import Footer from "../components/Footer";
import ReactStars from "react-rating-stars-component";
import { useState, useEffect } from "react";
import { collection, getDocs, orderBy, query } from "firebase/firestore";
import { db } from "../utils/firebase-config";
import { toast } from "react-toastify";

export default function CustomersReviews() {
  const [comments, setComments] = useState(null);

  useEffect(() => {
    async function fetchComments() {
      try {
        const commentsRef = collection(db, "customersReviews");
        const q = query(commentsRef, orderBy("data.timestamp", "desc"));
        const querySnap = await getDocs(q);

        const reviews = [];
        querySnap.forEach((doc) => {
          return reviews.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setComments(reviews);
      } catch (error) {
        toast.error("Pas de commentaires");
      }
    }
    fetchComments();
  }, []);

  return (
    <>
      <main className="max-w-7xl mx-auto min-h-screen shadow-lg">
        <h3 className="text-xl text-[#262526] font-semibold text-center uppercase my-6 relative">
          <span className="bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-2 inline-block">
            <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
          </span>
          Commentaires des clients
          <span className="bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-2 inline-block">
            <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
          </span>
        </h3>
        {comments && comments.length > 0 ? (
          <ul className="max-w-[680px] mx-auto p-4">
            {comments.map((comment, i) => (
              <li
              key={i}
              className="w-full bg-raisin border-b-2 border-smoke text-smoke"
              >
                <p className="font-semibold ml-4">
                  {comment.data.data.name} {comment.data.data.firstname}
                </p>
                <p className="ml-4 mt-2">
                  {comment.data.data.comment}
                </p>
                <div className="w-full flex justify-end items-center px-4">
                  <ReactStars 
                    count={5}
                    size={24}
                    value={comment.data.data.rating}
                    edit={false}
                  />
                </div>
              </li>
            ))}
          </ul>
        ) : null}
      </main>
      <Footer />
    </>
  );
}
