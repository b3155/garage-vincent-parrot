import React from "react";
import Experience from "../components/about/Experience";
import Experts from "../components/about/Experts";
import Professional from "../components/about/Professional";
import Footer from "../components/Footer";
import heroAbout from "../resources/heroAbout.jpg";
import { Helmet } from "react-helmet";

export default function About() {
  return (
    <>
      <Helmet>
        <title>À propos</title>
        <meta
          name="description"
          content="Faites l'expérience d'une Réparation automobile supérieure
          avec nous"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${heroAbout}) center / cover no-repeat`,
            }}
          >
            <h2 className="text-h2 text-smoke font-bold font-rajdhani text-center absolute bottom-20 md:text-start md:ml-20">
              Pourquoi nous choisir
            </h2>
          </div>
        </section>
        <Experience />
        <Professional />
        <Experts />
      </main>
      <Footer />
    </>
  );
}
