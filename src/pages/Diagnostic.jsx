import React from "react";
import Footer from "../components/Footer";
import NavServices from "../components/NavServices";
import hero from "../resources/hero-mecanique.jpg";
import mecanique from "../resources/mecanique.jpg";
import { Helmet } from "react-helmet";

export default function Maintenance() {
  return (
    <>
      <Helmet>
        <title>Mécanique de la voiture</title>
        <meta
          name="description"
          content="Mécanicien expert et de réparation. Le moteur est l'element clef de la mécanique d'une voiture. Diagnostic et réparation précis"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${hero}) center / cover no-repeat`,
            }}
          >
            <h2 className="text-h2 text-candyred font-bold font-rajdhani text-center absolute bottom-5 md:text-start md:ml-20">
              Mécanicien expert et<br></br>De réparation automobile
            </h2>
          </div>
        </section>
        <section>
          <div className="flex flex-col sm:flex-row py-20 px-10">
            <div className="sm:w-1/3">
              <NavServices />
            </div>
            <div className="flex justify-center">
              <div className="sm:w-2/3 mt-10">
                <div
                  className="w-full h-[300px] overflow-hidden"
                  style={{
                    background: `url(${mecanique}) center / cover no-repeat`,
                  }}
                ></div>
                <div className="p-4 text-raisin text-md font-medium">
                  <h4 className="text-xl text-raisin font-bold mb-4">
                    Mécanique de Précision
                  </h4>
                  <p>
                    Une voiture est un engin mécanique certe obscur pour
                    certains ! Un véhicule répond à un ensemble de mécanismes
                    fonctionnant les uns par rapport aux autres.
                  </p>
                  <p>
                    Le moteur est l'element clef de la mécanique d'une voiture
                    puisqu'il permet de démarrer et d'entrainer les roues plus
                    ou mois vite, à l'aide de la boite de vitesse .
                  </p>
                  <h4 className="text-xl text-raisin font-bold my-4">
                    Nos Avantages
                  </h4>
                  <p>- Diagnostic et réparation précis</p>
                  <p>- Service client et commodité</p>
                  <p>- Techniciens certifiés</p>
                  <p>- Détecter et résoudre des problèmes mineurs</p>
                  <p>
                    - Réduction des frais de pièces de rechanges et de services
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}
