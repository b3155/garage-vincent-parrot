import React from "react";
import { Link } from "react-router-dom";
import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import Spinner from "../components/Spinner";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../utils/firebase-config";
import { toast } from "react-toastify";
import Footer from "../components/Footer";

export default function SignIn() {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const inputs = useRef([]);
  const addInputs = (el) => {
    if (el && !inputs.current.includes(el)) {
      inputs.current.push(el);
    }
  };

  const formRef = useRef();

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);

    try {
      await signInWithEmailAndPassword(
        auth,
        inputs.current[0].value,
        inputs.current[1].value
      );

      setLoading(false);
      navigate("/dashboard");
    } catch (error) {
      toast.error("Email and/or password incorrect");
      // console.log(error);
    }
  }

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <section className="max-w-7xl mx-auto shadow-lg">
        <div className="flex min-h-screen bg-smoke justify-center items-center py-12 px-4 sm:px-6 lg:px-20 xl:px-24">
          <div className="w-full bg-raisin p-4 rounded-md max-w-md xl:mx-28 xxl:mx-32">
            <h2 className="mt-6 text-h3 text-candyred font-bold text-center">
              Se connecter
            </h2>
            <form ref={formRef} onSubmit={onSubmit} className="mt-4">
              <div className="space-y-5 mt-6">
                <div>
                  <label
                    htmlFor="email"
                    className="block text-md font-medium text-candyred"
                  >
                    Email
                  </label>
                  <input
                    ref={addInputs}
                    type="email"
                    id="email"
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="password"
                    className="block text-md font-medium text-candyred"
                  >
                    Password
                  </label>
                  <input
                    ref={addInputs}
                    type="password"
                    id="password"
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                  />
                </div>
                <div className="flex justify-end mt-8">
                  <Link to="/forgot-password">
                    <p className="mt-2 text-md text-candypink font-medium hover:text-candyred transition duration-150 ease-in-out cursor-pointer">
                      Password Oublie ?
                    </p>
                  </Link>
                </div>
              </div>
              <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                Se connecte
              </button>
            </form>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}
