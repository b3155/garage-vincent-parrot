import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router";
import { doc, getDoc } from "firebase/firestore";
import { auth, db } from "../utils/firebase-config";
import Spinner from "../components/Spinner";
import {
  FaChevronLeft,
  FaChevronRight,
  FaShare,
  FaMapMarkerAlt,
} from "react-icons/fa";
import Footer from "../components/Footer";

export default function Listing() {
  const params = useParams();
  const [listing, setListing] = useState(null);
  const [loading, setLoading] = useState(true);
  const [sliderIndex, setSliderIndex] = useState(0);
  const [shareLinkCopied, setShareLinkCopied] = useState(false);
  const [contactSeller, setContactSeller] = useState(false);

  useEffect(() => {
    async function fetchListing() {
      const docRef = doc(db, "listings", params.listingId);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        setListing(docSnap.data());
        setLoading(false);
      }
    }
    fetchListing();
  }, []);

  function toggleImage(index) {
    let newState;

    if (index + sliderIndex > listing.imgUrls.length - 1) {
      newState = 0;
    } else if (index + sliderIndex < 0) {
      newState = listing.imgUrls.length - 1;
    } else {
      newState = index + sliderIndex;
    }
    setSliderIndex(newState);
  }

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <main>
        <div className="max-w-7xl mx-auto">
          <div className="h-[550px] relative">
            {listing.imgUrls.map((url, index) => (
              <div
                key={index}
                className="w-full h-[520px] mx-auto overflow-hidden flex justify-between items-center absolute"
                style={{
                  background: `url(${listing.imgUrls[sliderIndex]}) center / cover no-repeat`,
                }}
              >
                <button
                  onClick={() => toggleImage(-1)}
                  className="w-8 h-8 relative left-10 bg-raisin border-2 border-smoke rounded-full"
                >
                  <FaChevronLeft className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 h-[14px] text-smoke" />
                </button>
                <button
                  onClick={() => toggleImage(1)}
                  className="w-8 h-8 relative right-10 bg-raisin border-2 border-smoke rounded-full"
                >
                  <FaChevronRight className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 h-[14px] text-smoke" />
                </button>
                <div
                  onClick={() => {
                    navigator.clipboard.writeText(window.location.href);
                    setShareLinkCopied(true);
                    setTimeout(() => {
                      setShareLinkCopied(false);
                    }, 2000);
                  }}
                  className="absolute top-10 right-[3%] z-10 bg-smoke border-2 border-raisin rounded-full w-10 h-10 flex justify-center items-center cursor-pointer"
                >
                  <FaShare className="text-lg text-raisin" />
                </div>
                {shareLinkCopied && (
                  <p className="absolute top-20 right-[17%] font-semibold border-2 border-raisin rounded-md bg-smoke z-10 p-2 sm:right-[10%]">
                    Link Copied
                  </p>
                )}
              </div>
            ))}
            <p className="absolute text-center text-h3 bg-white bottom-0 w-full">
              {sliderIndex + 1} / {listing.imgUrls.length}
            </p>
          </div>
          <div className="w-full">
            <h2 className="text-h2 text-center font-rajdhani font-semibold mt-6">
              {listing.brand} {listing.model}
            </h2>
            <div className="sm:flex p-4 bg-smoke space-x-4">
              <div className="mt-6 p-4 sm:p-10 sm:w-1/2">
                <p className="text-xl text-raisine font-semibold">
                  Caractéristiques :
                </p>
                <div className="m-6 sm:pl-6">
                  <p>Marque : {listing.brand}</p>
                  <p>Modèle : {listing.model}</p>
                  <p>Kilométrage : {listing.mileage}</p>
                  <p>Type de boite : {listing.gearbox}</p>
                  <p>Mise en circulation : {listing.release}</p>
                  <p>Puissance : {listing.power}</p>
                  <p>Nombres de places : {listing.placesNumber}</p>
                  <p>Nombres de portes : {listing.doorNumber}</p>
                  <p>Transmission : {listing.transmission}</p>
                </div>
                <p className="text-xl text-raisin font-semibold">
                  Equipements :
                </p>
                <div className="m-6 sm:pl-6">
                  <p>Couleur : {listing.color}</p>
                  <p>Option : {listing.option}</p>
                </div>
                <p className="text-xl text-raisin font-semibold">
                  Voiture Revisée et Garantie par Nos Services
                </p>
              </div>
              <div className="mt-6 p-4 sm:w-1/2 sm:p-10">
                <p className="flex items-baseline mb-3 font-semibold">
                  <FaMapMarkerAlt className="text-green-700 mr-2" />
                  <span className="text-raisin">{listing.address}</span>
                </p>
                <div className="space-y-4">
                  <p className="text-xl text-raisin font-semibold">
                    Description :
                  </p>
                  <p>{listing.description}</p>
                  {!auth.currentUser?.uid && !contactSeller && (
                    <div>
                      <p className="text-candyred font-medium text-center">
                        Contactez nous pour cet annoncée
                      </p>
                      <a
                        href={`mailto:${"parrotvincent@gmail.com"}?subject=${
                          listing.brand
                        }-${listing.model}`}
                      >
                        <button
                          onClick={() => setContactSeller(true)}
                          className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto mt-1 mb-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out"
                        >
                          Contacter Vincent{" "}
                          <span className="uppercase">Parrot</span>
                        </button>
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </>
  );
}
