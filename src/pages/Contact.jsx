import React from "react";
import ContactForm from "../components/ContactForm";
import Footer from "../components/Footer";
import contact from "../resources/contact.jpg";
import logo from "../assets/logo.png";
import { Helmet } from "react-helmet";

export default function Contact() {
  return (
    <>
      <Helmet>
        <title>Vincent Parrot -contact</title>
        <meta name="description" content="Envoyer message" />
      </Helmet>
      <section className="max-w-7xl mx-auto flex justify-center items-center min-h-screen shadow-lg">
        <div className="flex flex-wrap-reverse p-4 md:flex-nowrap md:gap-8 md:p-36">
          <div className="w-full rounded-md md:w-1/2">
            <ContactForm />
          </div>
          <div className="w-full rounded-md bg-raisin mb-4 md:w-1/2 md:mb-0">
            <div className="w-full h-[200px] overflow-hidden sm:h-[300px]">
              <img
                src={contact}
                alt="contact image"
                className="object-center/cover w-full h-full rounded-tl-md rounded-tr-md"
              />
            </div>
            <div className="flex flex-col justify-center items-center p-4 text-smoke font-semibold">
              <img src={logo} alt="logo" className="w-36" />
              <p className="mb-4">Toulouse</p>
              <p className="mb-4">Tel : ++33 865 234 234</p>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}
