import React from "react";
import { useState, useEffect } from "react";
import Footer from "../components/Footer";
import Spinner from "../components/Spinner";
import ListingItem from "../components/ListingItem";
import {
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  startAfter,
} from "firebase/firestore";
import { db } from "../utils/firebase-config";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

export default function Cars() {
  const [loading, setLoading] = useState(true);
  const [listings, setListings] = useState(null);
  const [lastFetchListing, setLastFetchListing] = useState(null);

  useEffect(() => {
    async function fetchListings() {
      try {
        const listingsRef = collection(db, "listings");
        const q = query(listingsRef, orderBy("timestamp", "desc"), limit(8));
        const querySnap = await getDocs(q);

        const lastVisible = querySnap.docs[querySnap.docs.length - 1];
        setLastFetchListing(lastVisible);

        const listings = [];
        querySnap.forEach((doc) => {
          return listings.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setListings(listings);
        setLoading(false);
      } catch (error) {
        toast.error("Impossible de récupérer les annonces");
      }
    }
    fetchListings();
  }, []);

  async function onFetchMoreListings() {
    setLoading(true);
    try {
      const listingsRef = collection(db, "listings");
      const q = query(
        listingsRef,
        orderBy("timestamp", "desc"),
        startAfter(lastFetchListing),
        limit(4)
      );
      const querySnap = await getDocs(q);

      const lastVisible = querySnap.docs[querySnap.docs.length - 1];
      setLastFetchListing(lastVisible);

      const listings = [];
      querySnap.forEach((doc) => {
        return listings.push({
          id: doc.id,
          data: doc.data(),
        });
      });
      setListings((prevState) => [...prevState, ...listings]);
      setLoading(false);
    } catch (error) {
      toast.error("Impossible de récupérer les annonces");
      setLoading(false);
      console.log(error);
    }
  }

  return (
    <>
      <Helmet>
        <title>Voiture occasion</title>
        <meta
          name="description"
          content="Garage Vincent Parrot vous propose un large choix de véhicules parmi les marques Volkswagen, Seat, Skoda, Audi, Honda, Hyundai."
        />
      </Helmet>
      <section className="max-w-7xl min-h-screen mx-auto shadow-lg p-6">
        <h2 className="relative text-h2 text-center font-semibold uppercase my-6">
          <span className="hidden md:inline-block bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-6">
            <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
          </span>
          Notre offre de voitures d'occasion
          <span className="hidden md:inline-block bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-6">
            <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
          </span>
        </h2>
        <div>
          <Link to="/filter">
            <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block my-8 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Ajouter un filter
            </button>
          </Link>
        </div>
        <div>
          {loading ? (
            <Spinner />
          ) : listings && listings.length > 0 ? (
            <>
              <main>
                <ul className="sm:grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
                  {listings.map((listing) => (
                    <ListingItem
                      key={listing.id}
                      id={listing.id}
                      listing={listing.data}
                    />
                  ))}
                </ul>
              </main>
              {lastFetchListing && (
                <div>
                  <button
                    onClick={onFetchMoreListings}
                    className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out"
                  >
                    Plus résultat
                  </button>
                </div>
              )}
            </>
          ) : (
            <p>Il n'y a aucune annonce actuelle</p>
          )}
        </div>
      </section>
      <Footer />
    </>
  );
}
