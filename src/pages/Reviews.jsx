import React from "react";
import Spinner from "../components/Spinner";
import Footer from "../components/Footer";
import { useState } from "react";
import { FaStar } from "react-icons/fa";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import logo from "../assets/logo.png";
import { db } from "../utils/firebase-config";
import { toast } from "react-toastify";
import { Helmet } from "react-helmet";

export default function Reviews() {
  const [name, setName] = useState("");
  const [firstname, setFirstname] = useState("");
  const [comment, setComment] = useState("");
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const [timestamp] = useState(serverTimestamp());

  const stars = Array(5).fill(0);

  const handleClick = (value) => {
    setRating(value);
  };

  const handleMouseHover = (value) => {
    setHover(value);
  };

  const handleMouseLeave = () => {
    setHover(undefined);
  };

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);

    try {
      const commentRef = collection(db, "reviews");
      await addDoc(commentRef, { name, firstname, comment, rating, timestamp });
      setLoading(false);

      setName("");
      setFirstname("");
      setComment("");
      setRating(0);
      toast.success("Votre commentaire est bien envoye");
    } catch (error) {
      toast.error("Votre commentaire ne pas envoye");
    }
  }

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <Helmet>
        <title>Envoyer Commentaire</title>
        <meta
          name="description"
          content="Votre avis nous interesse"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg py-12 px-4 md:px-24">
        <section>
          <h3 className="text-md text-raisin font-semibold text-center uppercase mt-6 relative sm:text-xl">
            <span className="bg-candyred w-6 h-1 absolute transform -translate-x-7 top-2 inline-block">
              <span className="bg-candyred w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
            </span>
            Votre avis nous interesse
            <span className="bg-candyred w-6 h-1 absolute transform translate-x-1 top-2 inline-block">
              <span className="bg-candyred w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
            </span>
          </h3>
          <div className="flex justify-center items-center my-24">
            <form
              onSubmit={onSubmit}
              className="max-w-[650px] bg-raisin p-4 sm:p-10"
            >
              <h4 className="mt-6 text-candyred text-2xl font-bold md:text-3xl lg:text4xl">
                Donne votre avis
              </h4>
              <div className="space-y-5 mt-6 p-4">
                <div>
                  <label
                    htmlFor="name"
                    className="block text-sm text-candyred font-medium"
                  >
                    Votre nom
                  </label>
                  <input
                    type="text"
                    id="name"
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
                  />
                </div>
                <div>
                  <label
                    htmlFor="firstname"
                    className="block text-sm text-candyred font-medium"
                  >
                    Votre prenom
                  </label>
                  <input
                    type="text"
                    id="firstname"
                    required
                    value={firstname}
                    onChange={(e) => setFirstname(e.target.value)}
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
                  />
                </div>
                <div>
                  <label
                    htmlFor="comment"
                    className="block text-sm text-candyred font-medium"
                  >
                    Votre commontaire
                  </label>
                  <textarea
                    type="text"
                    id="comment"
                    rows={4}
                    required
                    placeholder="Votre commentaire"
                    value={comment}
                    onChange={(e) => setComment(e.target.value)}
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
                  ></textarea>
                </div>
                <div className="w-full flex flex-col justify-center items-center pb-6">
                  <p className="text-md text-smoke font-semibold">
                    Votre note :
                  </p>
                  <div className="flex mt-2 space-x-2">
                    {stars.map((_, index) => {
                      return (
                        <FaStar
                          key={index}
                          value={rating}
                          color={
                            hover > index || rating > index
                              ? "d92332"
                              : "#f2f2f2"
                          }
                          onClick={() => handleClick(index + 1)}
                          onMouseOver={() => handleMouseHover(index + 1)}
                          onMouseLeave={handleMouseLeave}
                        />
                      );
                    })}
                  </div>
                </div>
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Envoyer Commentaire
                </button>
              </div>
              <div className="w-full flex justify-center items-center p-6">
                <img
                  src={logo}
                  alt="logo"
                  className="object-cover object-center w-[200px]"
                />
              </div>
            </form>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}
