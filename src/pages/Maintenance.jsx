import React from "react";
import Footer from "../components/Footer";
import NavServices from "../components/NavServices";
import maintenance from "../resources/maintenance.jpg";
import entretienRegulier from "../resources/entretien-regulier.jpg";
import { Helmet } from "react-helmet";

export default function Maintenance() {
  return (
    <>
      <Helmet>
        <title>Entretien Régulier</title>
        <meta
          name="description"
          content="L'entretien préventif et les réparations. Service client et commodité"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${maintenance}) center / cover no-repeat`,
            }}
          >
            <h2 className="text-h2 text-candyred font-bold font-rajdhani text-center absolute bottom-5 md:text-start md:ml-20">
              Professionnel d'entretien et<br></br>De réparation automobile
            </h2>
          </div>
        </section>
        <section>
          <div className="flex flex-col sm:flex-row py-20 px-10">
            <div className="sm:w-1/3">
              <NavServices />
            </div>
            <div className="flex justify-center">
              <div className="sm:w-2/3 mt-10">
                <div
                  className="w-full h-[300px] overflow-hidden"
                  style={{
                    background: `url(${entretienRegulier}) center / cover no-repeat`,
                  }}
                ></div>
                <div className="p-4 text-raisin text-md font-medium">
                  <h4 className="text-xl text-raisin font-bold mb-4">
                    Entretien Régulier
                  </h4>
                  <p>
                    L'entretien préventif et les réparations peuvent vous aider
                    à résoudre des pannes et des réparations couteuses à
                    l'avenir.
                  </p>
                  <p>
                    Un entretien régulier peut également vous aider à résoudre
                    de petits problèmes avant qu'ils ne deviennent des problèmes
                    importants et couteux.
                  </p>
                  <h4 className="text-xl text-raisin font-bold my-4">
                    Nos Avantages
                  </h4>
                  <p>- Diagnostic et réparation précis</p>
                  <p>- Service client et commodité</p>
                  <p>- Techniciens certifiés</p>
                  <p>- Détecter et résoudre des problèmes mineurs</p>
                  <p>
                    - Réduction des frais de pièces de rechanges et de services
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}
