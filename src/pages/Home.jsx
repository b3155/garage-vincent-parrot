import React from "react";
import ServicesSection from "../components/homecomponents/ServicesSection";
import AboutSection from "../components/homecomponents/AboutSection";
import Footer from "../components/Footer";
import hero from "../resources/hero-home.jpg";
import ReviewSection from "../components/homecomponents/ReviewSection";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

export default function Home() {
  return (
    <>
      <Helmet>
        <title>Vincent Parrot - Accueil</title>
        <meta
          name="description"
          content="Professionnel d'entretien et de réparation automobile. Opportunité de Service de Qualité. Ce que Nos Clients disent de Nous"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${hero}) center / cover no-repeat`,
            }}
          >
            <div className="flex flex-col w-full absolute top-1/2 transform -translate-y-1/2 md:pl-36">
              <h1 className="text-h1 text-smoke font-bold font-rajdhani text-center md:text-start">
                Professionnel d'entretien et<br></br>De réparation automobile
              </h1>
              <Link to="/contact">
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Contactez Nous
                </button>
              </Link>
            </div>
          </div>
        </section>
        <ServicesSection />
        <AboutSection />
        <ReviewSection />
      </main>
      <Footer />
    </>
  );
}
