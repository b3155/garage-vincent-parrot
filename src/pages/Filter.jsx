import React from "react";
import Footer from "../components/Footer";
import Spinner from "../components/Spinner";
import ListingItem from "../components/ListingItem";
import { useState, useEffect } from "react";
import { collection, getDocs, orderBy, query } from "firebase/firestore";
import { db } from "../utils/firebase-config";
import { toast } from "react-toastify";

export default function Filter() {
  const [listings, setListings] = useState(null);
  const [loading, setLoading] = useState(false);
  const [brand, setBrand] = useState("");
  const [year, setYear] = useState("");
  const [miles, setMiles] = useState("");
  const [price, setPrice] = useState("");
  const [isOpenBrand, setIsOpenBrand] = useState(false);
  const [isOpenYear, setIsOpenYear] = useState(false);
  const [isOpenMiles, setIsOpenMiles] = useState(false);
  const [isOpenPrice, setIsOpenPrice] = useState(false);

  useEffect(() => {
    async function fetchListings() {
      setLoading(true);

      try {
        const listingsRef = collection(db, "listings");
        const q = query(listingsRef, orderBy("timestamp", "desc"));
        const querySnap = await getDocs(q);

        const listings = [];
        querySnap.forEach((doc) => {
          return listings.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setListings(listings);
        setLoading(false);
      } catch (error) {
        toast.error("Y'a pas des annonces");
      }
    }
    fetchListings();
  }, []);

  const filterByBrand = (filteredData) => {
    if (!brand) {
      return filteredData;
    }
    const filteredCars = filteredData.filter(
      (car) => car.data.brand.indexOf(brand) !== -1
    );
    setIsOpenBrand(false);
    return filteredCars;
  };

  const filterByYear = (filteredData) => {
    if (!year) {
      return filteredData;
    }
    const filteredCars = filteredData.filter((car) => car.data.release < year);
    setIsOpenYear(false);
    return filteredCars;
  };

  const filterByMiles = (filteredData) => {
    if (!miles) {
      return filteredData;
    }
    const filteredCars = filteredData.filter((car) => car.data.mileage < miles);
    setIsOpenMiles(false);
    return filteredCars;
  };

  const filterByPrice = (filteredData) => {
    if (!price) {
      return filteredData;
    }
    const filteredCars = filteredData.filter((car) => car.data.price < price);
    setIsOpenPrice(false);
    return filteredCars;
  };

  const handleBrandChange = (e) => {
    setBrand(e.target.value);
  };

  const handleYearChange = (e) => {
    const selectedYear = Number(e.target.id);

    if (selectedYear === year) {
      setYear("");
    } else {
      setYear(selectedYear);
    }
  };

  const handleMilesChange = (e) => {
    const selectedMiles = Number(e.target.id);

    if (selectedMiles === miles) {
      setMiles("");
    } else {
      setMiles(selectedMiles);
    }
  };

  const handlePriceChange = (event) => {
    const selectedPrice = Number(event.target.id);

    if (selectedPrice === price) {
      setPrice("");
    } else {
      setPrice(selectedPrice);
    }
  };

  useEffect(() => {
    let filteredData = filterByBrand(listings);
    filteredData = filterByYear(filteredData);
    filteredData = filterByMiles(filteredData);
    filteredData = filterByPrice(filteredData);
    setListings(filteredData);
  }, [brand, year, miles, price]);

  const resetBtn = () => {
    async function fetchListings() {
      setLoading(true);

      try {
        const listingsRef = collection(db, "listings");
        const q = query(listingsRef, orderBy("timestamp", "desc"));
        const querySnap = await getDocs(q);

        const listings = [];
        querySnap.forEach((doc) => {
          return listings.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setListings(listings);
        setBrand("");
        setYear("");
        setMiles("");
        setPrice("");
        setLoading(false);
      } catch (error) {
        toast.error("Y'a pas des listings");
      }
    }
    fetchListings();
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <div className="md:flex max-w-7xl mx-auto min-h-screen">
        <div className="md:w-1/4 space-y-2 p-4">
          <h3 className="text-2xl uppercase text-candyred font-semibold mb-6">
            Filter
          </h3>
          <div>
            <button
              onClick={() => setIsOpenYear((prevState) => !prevState)}
              className="font-bold border-b border-raisin mb-2"
            >
              Filter by Year
            </button>
            {isOpenYear && (
              <ul onClick={handleYearChange}>
                <li
                  id="2010"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Avant : 2010
                </li>
                <li
                  id="2015"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Avant : 2015
                </li>
                <li
                  id="2020"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Avant : 2020
                </li>
                <li
                  id="2023"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Avant : 2023
                </li>
              </ul>
            )}
          </div>
          <div>
            <button
              onClick={() => setIsOpenMiles((prevState) => !prevState)}
              className="font-bold border-b border-raisin mb-2"
            >
              Filter by Kilometrage
            </button>
            {isOpenMiles && (
              <ul onClick={handleMilesChange}>
                <li
                  id="50000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de : 50000 km
                </li>
                <li
                  id="100000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de : 100000 km
                </li>
                <li
                  id="150000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de : 150000 km
                </li>
              </ul>
            )}
          </div>
          <div>
            <button
              onClick={() => setIsOpenPrice((prevState) => !prevState)}
              className="font-bold border-b border-raisin mb-2"
            >
              Filter by Price
            </button>
            {isOpenPrice && (
              <ul onClick={handlePriceChange}>
                <li
                  id="10000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de 10000 euros
                </li>
                <li
                  id="15000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de 15000 euros
                </li>
                <li
                  id="20000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de 20000 euros
                </li>
                <li
                  id="25000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de 25000 euros
                </li>
                <li
                  id="30000"
                  className="text-raisin hover:text-alizarin cursor-pointer"
                >
                  Mois de 30000 euros
                </li>
              </ul>
            )}
          </div>
          <div>
            <button
              onClick={() => setIsOpenBrand((prevState) => !prevState)}
              className="font-bold border-b border-raisin mb-2"
            >
              Filter by Brand
            </button>
            {isOpenBrand && (
              <>
                <select
                  onChange={handleBrandChange}
                  className="block border border-raisin rounded"
                >
                  <option value="">All Cars</option>
                  {listings.map((listing, i) => (
                    <option key={i} value={listing.data.brand}>
                      {listing.data.brand}
                    </option>
                  ))}
                </select>
              </>
            )}
          </div>
          <button
            onClick={resetBtn}
            className="px-4 py-1 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out"
          >
            Reset filter
          </button>
        </div>
        <div>
          {listings && listings.length > 0 ? (
            <>
              <ul className="sm:grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 p-4">
                {listings.map((item) => (
                  <ListingItem
                    key={item.id}
                    id={item.id}
                    listing={item.data}
                    className="max-h-[320px]"
                  />
                ))}
              </ul>
            </>
          ) : (
            <p>Y'a pas des annonces disponible pour votre recherche</p>
          )}
        </div>
      </div>
      <Footer />
    </>
  );
}
