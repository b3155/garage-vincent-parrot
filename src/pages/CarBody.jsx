import React from "react";
import Footer from "../components/Footer";
import NavServices from "../components/NavServices";
import hero from "../resources/hero-carrosserie.jpg";
import carrosserie from "../resources/main-carrosserie.jpg";
import { Helmet } from "react-helmet";

export default function CarBody() {
  return (
    <>
      <Helmet>
        <title>Carrosserie</title>
        <meta
          name="description"
          content="Faire le choix de la qualité et de la rapidité. Confiez nous votre véhicule pour le retrouver comme au premier jour"
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${hero}) center / cover no-repeat`,
            }}
          >
            <h2 className="text-h2 text-candyred font-bold font-rajdhani text-center absolute bottom-5 bg-smoke p-2 md:text-start md:ml-20">
              Carrosserie
            </h2>
          </div>
        </section>
        <section>
          <div className="flex flex-col sm:flex-row py-20 px-10">
            <div className="sm:w-1/3">
              <NavServices />
            </div>
            <div className="flex justify-center">
              <div className="sm:w-2/3 mt-10">
                <div
                  className="w-full h-[300px] overflow-hidden"
                  style={{
                    background: `url(${carrosserie}) center / cover no-repeat`,
                  }}
                ></div>
                <div className="p-4 text-raisin text-md font-medium">
                  <h4 className="text-xl text-raisin font-bold mb-4">
                    Carrosserie
                  </h4>
                  <p>
                    Choisir les carrosseries VINCENT Parrot, c'est faire le
                    choix de la qualité et de la rapidité, quelle que soit votre
                    assurance grace au libre choix du réparateur.
                  </p>
                  <p>
                    Confiez nous votre véhicule pour le retrouver comme au
                    premier jour !
                  </p>
                  <h4 className="text-xl text-raisin font-bold my-4">
                    Nos Avantages
                  </h4>
                  <p>- Diagnostic et réparation précis</p>
                  <p>- Rayures</p>
                  <p>- Raccord peinture</p>
                  <p>- Débosselage</p>
                  <p>- Réparation de jantes</p>
                  <p>- Service Carrosserie Express</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}
