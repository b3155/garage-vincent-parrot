import React from "react";
import Footer from "../components/Footer";

export default function PrivacyPolicy() {
  return (
    <>
      <section className="max-w-7xl mx-auto shadow-lg">
        <div className="max-w-6xl mx-auto p-24 text-md">
          <h3 className="text-xl text-[#262526] font-semibold mb-6">
            Politique de condientialité et des cookies
          </h3>
          <p>
            La politique de confidentialité suivante a pour but d'informer sur
            la manière dont sont traitées les données qui nous sont confiées.
            Cette démarche s'inscrit dans le respect de la "Loi informatique et
            Liberté" du 6 Janvier 1978 modifiée et du "Règlement Européen
            relatif à la protection des données personnelles" (le "RGPD") du 27
            Avril 2016. Vous trouverez de plus amples informations sur ces
            normes en visitant le site de la CNIL (
            <a
              className="text-[#d97774] hover:text-[#d92332]"
              href="https://www.cnil.fr"
              target="blanc_"
            >
              www.cnil.fr
            </a>
            ).{" "}
          </p>
          <p>
            La présente politique de confidentialité pourra évoluer en fonction
            du contexte légal et réglementaire et de la doctrine de la comission
            nationale de l'informatique et des Libertés. (CNIL)
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">
            Durée de conservation des données
          </h4>
          <p>
            Vos données sont conservées le temps strictement nécessaire afin
            d'aporter une réponse à votre demande. Lorsque ce n'est plus
            nécessaire, vos données personnlles sont supprimées ou rendues
            anonymes.{" "}
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">
            Utilisation des cookies pour l'ensemble des sites web
          </h4>
          <p>
            Les "cookies" sont de petits fichiers textes envoyés à votre
            terminal et stockés sur son disque dur pour permettre à nos sites
            internet de vous reconnaitre lorsque vous le visitez. Lorsque vous
            visitez notre site internet, nous pouvons sous réserve de votre
            consentement, utiliser des cookies, des pixels témoins et d'autres
            technologies pour recueillir les données suivantes :{" "}
          </p>
          <ul className="space-y-5 mt-2">
            <li>
              1. Adresse IP, informations de connexion, type de navigateur,
              emplacement, fuseau horaires, système d'exploitation et d'autres
              informations techniques.{" "}
            </li>
            <li>
              2. Informations concernant votre visite, y compris les sites
              internet que vous visitez avant et après notre site internet et
              les produits que vous avez consultés.{" "}
            </li>
            <li>
              3. Durée des visites sur les pages de nos sites web et
              informations relatives à l'interation entre les pages.{" "}
            </li>
          </ul>
        </div>
      </section>
      <Footer />
    </>
  );
}
