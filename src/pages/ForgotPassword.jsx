import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router";
import { auth }from "../utils/firebase-config"
import { sendPasswordResetEmail } from "firebase/auth"
import { toast } from "react-toastify"
import Spinner from "../components/Spinner"
import Footer from "../components/Footer";

export default function ForgotPassword() {

  const [email, setEmail] = useState("")
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()

  async function onSubmit(e) {
    e.preventDefault()
    setLoading(true)

    try {
      await sendPasswordResetEmail(auth, email)
      toast.success("Email a été envoyé")
      navigate("/sign-in")
    } catch (error) {
      toast.error("Impossible d'envoyer le mot de passe de réinitialisation")
    }
  }

  if(loading) {
    return <Spinner />
  }

  return (
    <section>
      <div
      className="min-h-[845px] max-w-7xl mx-auto bg-smoke flex justify-center items-center py-12 px-4 sm:px-6 lg:px-20 xl:px-24"
      >
        <div
        className="w-full bg-raisin p-4 rounded-md max-w-md "
        >
          <h2
          className="mt-6 text-h2 text-candyred font-semibold text-center"
          >
            Password Oublie
          </h2>
          <form
          onSubmit={onSubmit}
          className="mt-4"
          >
            <div className="space-y-5 mt-6">
              <label 
              htmlFor="email"
              className="block text-md text-candyred font-medium"
              >
                Email adresse
              </label>
              <input 
              type="email" 
              id="email" 
              required
              value={email}
              onChange={() => setEmail(e.target.value)} 
              className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
              />
            </div>
            <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-6 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Send reset password
            </button>
          </form>
        </div>
      </div>
      <Footer />
    </section>
  );
}
