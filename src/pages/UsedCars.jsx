import React from "react";
import Footer from "../components/Footer";
import NavServices from "../components/NavServices";
import hero from "../resources/hero-occasion.jpg";
import audi from "../resources/audi.jpg";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

export default function Maintenance() {
  return (
    <>
      <Helmet>
        <title>Véhicules d'occasion</title>
        <meta
          name="description"
          content="Sérénité, tranquillité : tous nos véhicules d'occasion sont révisés et disposent d'une garantie, quelle que soit la marque."
        />
      </Helmet>
      <main className="max-w-7xl mx-auto shadow-lg">
        <section>
          <div
            className="relative w-full h-[450px] overflow-hidden"
            style={{
              background: `url(${hero}) center / cover no-repeat`,
            }}
          >
            <h2 className="text-h2 text-candyred font-bold font-rajdhani text-center absolute bottom-5 md:text-start md:ml-20">
              Achète une voiture d'occasion
            </h2>
          </div>
        </section>
        <section>
          <div className="flex flex-col sm:flex-row py-20 px-10">
            <div className="sm:w-1/3">
              <NavServices />
            </div>
            <div className="flex justify-center">
              <div className="sm:w-2/3 mt-10">
                <div
                  className="w-full h-[300px] overflow-hidden"
                  style={{
                    background: `url(${audi}) center / cover no-repeat`,
                  }}
                ></div>
                <div className="p-4 text-raisin text-md font-medium">
                  <h4 className="text-xl text-raisin font-bold mb-4">
                    Véhicules d'occasion
                  </h4>
                  <p>
                    Retrouvez plus d'une centaine de véhicules d'occasion toutes
                    marques et tous budgets dans votre concession Vincent Parrot
                    à Toulouse .
                  </p>
                  <h4 className="text-xl text-raisin font-bold my-4">
                    Nos Avantages
                  </h4>
                  <p>- Véhicules révisés et garantis</p>
                  <p>- Véhicules avec Controle Technique</p>
                  <p>- Techniciens certifiés</p>
                  <p>- Concessionnaire approuvé</p>
                  <p>- Véhicules Garantis</p>
                </div>
                <Link to="/cars">
                  <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                    Trouver votre Véhicule
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}
