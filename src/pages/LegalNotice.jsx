import React from "react";
import Footer from "../components/Footer";

export default function LegalNotice() {
  return (
    <>
      <section className="max-w-7xl mx-auto shadow-lg">
        <div className="max-w-6xl mx-auto p-24 text-md">
          <h3 className="text-xl text-[#262526] font-semibold mb-6">
            Propriété
          </h3>
          <p>Le présent site web est édité par :</p>
          <p>Garage Vincent PARROT dont le siège est situé :</p>
          <p>TOULOUSE</p>
          <p>France</p>
          <p>Tél : ++33 865 234 234</p>
          <p>
            Adresse de courrier électronique :{" "}
            <span className="text-[#d92332]">parrotvincent@gmail.com</span>{" "}
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">
            Directeur de publication
          </h4>
          <p>
            Le responsable de la publication du site web est Monsieur Vincent
            PARROT.{" "}
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">
            Conditions générales d'utilisation
          </h4>
          <p>
            L'utilisateur reconnait avoir pris connaissance des présentes
            conditions d'utilisation et s'engage à le respecter.
          </p>
          <p>
            L'utilisateur du site internet "vincentparrot.com" reconnait
            disposer des compétences et des moyens nécessaires pour accéder et
            utiliser ces sites.{" "}
          </p>
          <p>
            L'utilisateur du site internet "vincentparrot.com" reconnait avoir
            vérifié que la configuration informatique utilisée ne contient aucun
            virus et qu'elle est en parfait état de fonctionnement.{" "}
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">
            Protection des données personnelles
          </h4>
          <p>
            Le traitement des données personnelles réalisé à partir du site web
            "vincentparrot.com" sont en conformité avec les exigences légales
            applicables, et notamment la loi numéro 78-17 du 6 Janvier 1978
            modifiée, relatives à l'informatique, aux fichiers et aux libertés
            et le règlement général sur la protection des données personnelles
            du 27 Avril 2016 )RGPD).{" "}
          </p>
          <h4 className="text-lg text-[#262526] font-semibold my-6">Cookies</h4>
          <p>
            Un cookie )ou témoin de connexion) est un fichier texte susceptible
            d'être enregistré, sous réserve de vos choix, dans un espace dédié
            du disque dur de votre terminal )ordinateur, tablette...) à
            l'occasion de la consultation d'un service en ligne grâce à votre
            navigateur.{" "}
          </p>
        </div>
      </section>
      <Footer />
    </>
  );
}
