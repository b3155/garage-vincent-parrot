import React from "react";
import CreateUser from "./CreateUser";
import { useState, useEffect } from "react";
import { collection, deleteDoc, doc, getDocs, limitToLast, query } from "firebase/firestore";
import { db } from "../../utils/firebase-config";
import { toast } from "react-toastify"

export default function Employee() {
  const [employee, setEmployee] = useState(null);

  useEffect(() => {
    async function fetchEmployees() {
      try {
        const employeeRef = collection(db, "employees");
        const q = query(employeeRef);
        const querySnap = await getDocs(q);

        const employee = [];
        querySnap.forEach((doc) => {
          return employee.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setEmployee(employee);
      } catch (error) {
        toast.error("Y'a pas des employees");
      }
    }
    fetchEmployees();
  }, []);

  async function deleteEmployee(employeID) {
    if(window.confirm("Étes-vous sûr de vouloir supprimer cet employé")) {
      await deleteDoc(doc(db, "employees", employeID))

      const updateListingEmployee = employee.filter((employe) => employe.id !== employeID)
      
      setEmployee(updateListingEmployee)
      toast.success("Supprimer avec succès la liste")
    }
  }

  return (
    <>
      <div className="flex justify-center mt-8">
        <table className="bg-smoke sm:min-w-[390px] md:min-w-[480px]">
          <thead>
            <tr className="w-full bg-raisin text-smoke">
              <th className="text-start p-2">Nom et Prenom</th>
              <th className="text-start p-2">Fonction</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {employee && employee.length > 0 ? (
              <>
                {employee.map((employe, i) => (
                  <tr key={i} className="border-b border-raisin">
                    <th className="text-start p-2">
                      {employe.data.firstname} {employe.data.name}
                    </th>
                    <td className="text-start p-2">
                      {employe.data.profession}
                    </td>
                    <td className="text-start p-2">
                      <button
                        onClick={() => deleteEmployee(employe.id)}
                        type="button"
                        className="text-candyred font-semibold cursor-pointer hover:text-candypink"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
              </>
            ) : null}
          </tbody>
        </table>
      </div>
      <CreateUser />
    </>
  );
}
