import React from "react"
import { doc, setDoc, Timestamp } from "firebase/firestore"
import Spinner from "../Spinner"
import { useState } from "react"
import { createUserWithEmailAndPassword } from "firebase/auth"
import { auth, db } from "../../utils/firebase-config"
import { toast } from "react-toastify"


export default function CreateUser() {
   
  const [loading, setLoading] = useState(false)

  const [formData, setFormData] = useState({
    name: "",
    firstname: "",
    email: "",
    age: 0,
    profession: "",
    startdate: "",
    password: "",
    role: 1,
    time: Timestamp.now()
  })
  const {
    name,
    firstname,
    email,
    age,
    profession,
    startdate,
    password,
    role,
    time
  } = formData

  function onChange(e) {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value
    }))
  }

  async function onSubmit(e) {
    e.preventDefault()
    setLoading(true)
    
    try {
      const userCredentials = await createUserWithEmailAndPassword(auth, email, password)
      
      const user = userCredentials.user
      await setDoc(doc(db, "employees", user.uid), formData)
      setLoading(false)
      setFormData("")
      toast.success("Salarié est bien enregistre")
      
    } catch (error) {
      setLoading(false)
      if(error.code === "auth/invalid-email") {
        toast.error("Format d'email invalide")
      } else if(error.code === "auth/email-already-in-use") {
        toast.error("Adresse email déjà utilisée")
      } else if(error.code === "auth/week-password") {
        toast.error("Mot de passe 6 caracters min")
      } else{
        toast.error("Quelque chose s'est mal passé avec l'enregistrement")
      }
    }
  }

  if(loading) {
    return <Spinner />
  }


  return (
    <div className="flex justify-center">
      <form 
      onSubmit={onSubmit}
      className="mt-24 max-w-[480px] bg-raisin p-4"
      >
        <div className="space-y-5 mt-6">
          <div className="flex space-x-10">
            <div>
              <label
                htmlFor="name"
                className="block text-sm font-medium text-candyred"
              >
                Nom
              </label>
              <input
                type="text"
                id="name"
                required
                value={name}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
            <div>
              <label
                htmlFor="firstname"
                className="block text-sm font-medium text-candyred"
              >
                Prenom
              </label>
              <input
                type="text"
                id="firstname"
                required
                value={firstname}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
          </div>
          <div className="flex space-x-10">
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium text-candyred"
              >
                Email adresse
              </label>
              <input
                type="email"
                id="email"
                required
                value={email}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
            <div>
              <label
                htmlFor="age"
                className="block text-sm font-medium text-candyred"
              >
                Age
              </label>
              <input
                type="number"
                id="age"
                required
                value={age}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
          </div>
          <div className="flex space-x-10">
            <div>
              <label
                htmlFor="proffesion"
                className="block text-sm font-medium text-candyred"
              >
                Profession
              </label>
              <input
                type="text"
                id="profession"
                required
                value={profession}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
            <div>
              <label
                htmlFor="startdate"
                className="block text-sm font-medium text-candyred"
              >
                Date de commencement
              </label>
              <input
                type="text"
                id="startdate"
                required
                value={startdate}
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
              />
            </div>
          </div>
          <div>
            <label
              htmlFor="password"
              className="block text-sm font-medium text-candyred"
            >
              Créer un mot de passe
            </label>
            <input
              type="password"
              id="password"
              required
              value={password}
              onChange={onChange}
              className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-sm"
            />
          </div>
        </div>
        <button 
        className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-6 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out"
        >
          Rajouter un salarié
        </button>
      </form>
    </div>
  );
}
