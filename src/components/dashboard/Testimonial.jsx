import React from "react";
import { useState, useEffect } from "react";
import { addDoc, collection, deleteDoc, doc, getDocs, orderBy, query } from "firebase/firestore";
import { db } from "../../utils/firebase-config";
import ReactStars from "react-rating-stars-component";
import { toast } from "react-toastify";
import Spinner from "../../components/Spinner"
import { FaTrash } from "react-icons/fa"

export default function Testimonial() {
  const [reviews, setReviews] = useState(null);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    async function fetchComments() {
      try {
        const commentRef = collection(db, "reviews");
        const q = query(commentRef, orderBy("timestamp", "desc"));
        const querySnap = await getDocs(q);

        const comments = [];
        querySnap.forEach((doc) => {
          return comments.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setReviews(comments);
      } catch (error) {
        toast.error("Pas de commentaires");
      }
    }
    fetchComments();
  }, []);

  async function addReview(data) {
    setLoading(true)

    try {
      const commentRef = collection(db, "customersReviews")
      await addDoc(commentRef, {data})
      setLoading(false)
      toast.success("Commentaire est bien ajouter dans page d'accueil")
    } catch (error) {
      toast.error("Commentaire ne pas ajouter")
    }
  }

  async function onDelete(commentId) {
    if(window.confirm("Etes-vous sûr que vous voulez supprimer cet commentaire")) {
      await deleteDoc(doc(db, "reviews", commentId))

      const updateComments = reviews.filter(
        (comment) => comment.id !== commentId
      )

      setReviews(updateComments)
      toast.success("Supprimer avec succès le commentaire")
    }
  }

  if(loading) {
    return <Spinner />
  }

  return (
    <>
      {reviews && reviews.length > 0 ? (
        <ul className="p-4">
          {reviews.map((comment, i) => (
            <li key={i} className="w-full border border-raisin mb-4">
              <h3 className="bg-smoke pl-8 mb-2">
                {comment.data.timestamp.toDate().toLocaleDateString("fr-FR")}
              </h3>
              <p className="text-md text-raisin font-semibold ml-4">
                From : {comment.data.name} {comment.data.firstname}
              </p>
              <p className="text-md text-raisin ml-4 mt-2">
                {comment.data.comment}
              </p>
              <div className="z-1">
                <ReactStars
                  count={5}
                  size={24}
                  value={comment.data.rating}
                  edit={false}
                />
              </div>
              <div className="flex justify-end items-center">
                <button 
                onClick={() => addReview(comment.data)}
                className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Ajouter
                </button>
                {onDelete && (
                  <FaTrash 
                  onClick={() => onDelete(comment.id)}
                  className="h-[24px] text-candypink hover:text-candyred mx-2 cursor-pointer"
                  />
                )}
              </div>
            </li>
          ))}
        </ul>
      ) : null}
    </>
  );
}
