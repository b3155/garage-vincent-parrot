import React from 'react'
import { useState, useEffect } from 'react'
import { collection, query, orderBy, getDocs, deleteDoc, doc} from "firebase/firestore"
import { db } from "../../utils/firebase-config"
import { toast } from "react-toastify"
import { FaTrash, FaEdit } from "react-icons/fa"
import { useNavigate } from 'react-router'


export default function AllCars() {
  const [listings, setListings] = useState(null)
  const navigate = useNavigate()

  useEffect(() => {
    async function fetcListings() {
      try {
        const listingsRef = collection(db, "listings")
        const q = query(listingsRef, orderBy("timestamp", "desc"))
        const querySnap = await getDocs(q)

        const listings = []
        querySnap.forEach((doc) => {
          return listings.push({
            id: doc.id,
            data: doc.data()
          })
        })
        setListings(listings)
      } catch (error) {
        toast.error("Y'a pas des listings")
      }
    }
    fetcListings()
  }, [])

  async function onDelete(listingId) {
    if(window.confirm("Ètes-vous sûr de vouloir supprimer la fiche")) {
      await deleteDoc(doc(db, "listings", listingId))

      const updatedListings = listings.filter((listing) => listing.id !== listingId)

      setListings(updatedListings)
      toast.success("Supprimer avec succès la liste")
    }
  }

  function onEdit(listingId) {
    navigate(`/edit-listing/${listingId}`)
  }

  return (
    <>
      <div className='flex justify-center mt-8'>
        <table className='bg-smoke min-w-[300px] md:min-w-[480px]'>
          <thead>
            <tr className='w-full bg-raisin text-smoke'>
              <th className='text-start p-2'>Marque</th>
              <th className='text-start p-2'>Modèle</th>
              <th className='text-start p-2'>Année</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {listings && listings.length > 0 ? (
              <>
                {listings.map((listing, i) => (
                  <tr 
                  key={i}
                  className="border-b border-raisin"
                  >
                    <th className='text-start p-2'>
                      {listing.data.brand}
                    </th>
                    <td className='text-start p-2'>
                      {listing.data.model}
                    </td>
                    <td className='text-start p-2'>
                      {listing.data.release}
                    </td>
                    <td className='p-2 flex justify-end items-baseline'>
                      {onEdit && (
                        <FaEdit 
                        onClick={() => onEdit(listing.id)}
                        className="hidden h-4 text-raisin cursor-pointer sm:flex"
                        />
                      )}
                      {onDelete && (
                        <FaTrash 
                        onClick={() => onDelete(listing.id)}
                        className="hidden h-[14px] text-candyred ml-2 cursor-pointer sm:flex"
                        />
                      )}
                    </td>
                  </tr>
                ))}
              </>
            ) : null}
          </tbody>
        </table>
      </div>
    </>
  )
}
