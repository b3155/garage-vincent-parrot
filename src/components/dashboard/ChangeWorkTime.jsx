import { doc, updateDoc } from "firebase/firestore";
import React from "react";
import { useState } from "react";
import { db } from "../../utils/firebase-config";
import Spinner from "../Spinner"
import { toast } from "react-toastify"
import WorkTime from "./WorkTime";

export default function ChangeWorkTime() {

  const [loading, setLoading] = useState(false)

  const [weekTime, setWeekTime] = useState({
    morningTime: "",
    afternoonTime: "",
    saturdayMorningTime: "",
    saturdayAfternoonTime: "",
    sundayMorningTime: "",
    sundayAfternoonTime: ""
  })
  const {
    morningTime,
    afternoonTime,
    saturdayMorningTime,
    saturdayAfternoonTime,
    sundayMorningTime,
    sundayAfternoonTime
  } = weekTime

  function onChange(e) {
    setWeekTime((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value
    }))
  }

  async function handleWeekTime(e) {
    e.preventDefault()
    setLoading(true)

    const docRef = doc(db, "weeksTimes", "3Gl2vcFqzEbMKTFQAJs3")
    await updateDoc(docRef, {
      morningTime: morningTime,
      afternoonTime: afternoonTime,
      saturdayMorningTime: saturdayMorningTime,
      saturdayAfternoonTime: saturdayAfternoonTime,
      sundayMorningTime: sundayMorningTime,
      sundayAfternoonTime: sundayAfternoonTime
    })

    toast.success("Des horaires sont bien mise à jour")
    setLoading(false)
    setWeekTime("")
  }

  if(loading) {
    return <Spinner />
  }

  return (
    <>
      <WorkTime />

      <div className="flex flex-col items-center">
        <form
        onSubmit={handleWeekTime}
        className="mt-12 max-w-[480px] bg-raisin p-4 space-y-5"
        >
          <div className="">
            <h2
            className="text-2xl text-smoke font-semibold mb-10"
            >
              Horaires du Lundi au Vendredi
            </h2>
            <div className="flex space-x-4">
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="morningTime"
                className="block text-md font-medium text-candyred"
                >
                  Matin
                </label>
                <input 
                type="text" 
                id="morningTime" 
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="afternoonTime"
                className="block text-md font-medium text-candyred"
                >
                  Apres midi
                </label>
                <input 
                type="text" 
                id="afternoonTime"
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
            </div>
          </div>
          <div className="">
            <h2
            className="text-2xl text-smoke font-semibold mb-10"
            >
              Horaires de samedi
            </h2>
            <div className="flex space-x-4">
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="saturdayMorningTime"
                className="block text-md font-medium text-candyred"
                >
                  Matin
                </label>
                <input 
                type="text" 
                id="saturdayMorningTime" 
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="saturdayAfternoonTime"
                className="block text-md font-medium text-candyred"
                >
                  Apres midi
                </label>
                <input 
                type="text" 
                id="saturdayAfternoonTime"
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
            </div>
          </div>
          <div className="">
            <h2
            className="text-2xl text-smoke font-semibold mb-10"
            >
              Horaires de dimanche
            </h2>
            <div className="flex space-x-4">
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="sundayMorningTime"
                className="block text-md font-medium text-candyred"
                >
                  Matin
                </label>
                <input 
                type="text" 
                id="sundayMorningTime" 
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
              <div className="border border-smoke rounded-md p-2">
                <label 
                htmlFor="sundayAfternoonTime"
                className="block text-md font-medium text-candyred"
                >
                  Apres midi
                </label>
                <input 
                type="text" 
                id="sundayAfternoonTime"
                required 
                onChange={onChange}
                className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md"
                />
              </div>
            </div>
          </div>
          <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
            Changer d'horaires
          </button>
        </form>
      </div>
    </>
  );
}
