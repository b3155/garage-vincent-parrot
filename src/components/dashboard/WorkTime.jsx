import React from "react";
import { useState, useEffect } from "react";
import { FaRegClock } from "react-icons/fa";
import { collection, getDocs, query } from "firebase/firestore";
import { db } from "../../utils/firebase-config";
import { toast } from "react-toastify";

export default function WorkTime() {
  const [worksTimes, setWorksTimes] = useState(null);

  useEffect(() => {
    async function fetchWorkTime() {
      try {
        const timeRef = collection(db, "weeksTimes");
        const q = query(timeRef);
        const querySnap = await getDocs(q);

        const workTime = [];
        querySnap.forEach((doc) => {
          return workTime.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setWorksTimes(workTime);
      } catch (error) {
        toast.error("Y'a pas des horaires à afficher");
        console.log(error);
      }
    }
    fetchWorkTime();
  }, []);

  return (
    <>
      <div className="bg-raisin flex flex-col">
        <div className="flex items-baseline">
          <FaRegClock className="text-candyred w-[20px]" />
          <p className="text-smoke text-sm ml-2 mb-2">Horaires d'ouverture</p>
        </div>
        {worksTimes && worksTimes.length > 0 ? (
          <>
            <div className="flex text-end">
              <p className="text-smoke">Lun - Ven :</p>
              <p className="ml-2 text-smoke mb-1">
                <span>{worksTimes[0].data.morningTime}</span>
                <br />
                <span>{worksTimes[0].data.afternoonTime}</span>
              </p>
            </div>
            <div className="flex text-end">
              <p className="text-smoke">Samedi :</p>
              <p className="ml-2 text-smoke mb-1">
                <span>{worksTimes[0].data.saturdayMorningTime}</span>
                <br />
                <span>{worksTimes[0].data.saturdayAfternoonTime}</span>
              </p>
            </div>
            <div className="flex text-end">
              <p className="text-smoke">Dimanche :</p>
              <p className="ml-2 text-smoke mb-1">
                <span>{worksTimes[0].data.sundayAfternoonTime}</span>
                <br />
                <span>{worksTimes[0].data.sundayAfternoonTime}</span>
              </p>
            </div>
          </>
        ) : null}
      </div>
    </>
  );
}
