import React from "react";
import { useState, useEffect } from "react";
import Spinner from "../Spinner";
import { toast } from "react-toastify";
import { auth, db, storage } from "../../utils/firebase-config";
import { v4 as uuidv4 } from "uuid";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import {
  addDoc,
  collection,
  getDocs,
  orderBy,
  query,
  serverTimestamp,
} from "firebase/firestore";
import ListingItem from "../ListingItem";
import { useNavigate } from "react-router";

export default function NewCar() {
  const [loading, setLoading] = useState(false);
  const [listings, setListings] = useState(null);
  const navigate = useNavigate()

  const [formData, setFormData] = useState({
    brand: "",
    model: "",
    mileage: "",
    price: "",
    gearbox: "",
    release: "",
    power: "",
    doorNumber: 0,
    placesNumber: 0,
    transmission: "",
    color: "",
    option: "",
    images: {},
    description: "",
  });
  const {
    brand,
    model,
    mileage,
    price,
    gearbox,
    release,
    power,
    doorNumber,
    placesNumber,
    transmission,
    color,
    option,
    images,
    description,
  } = formData;

  function onChange(e) {
    if (!e.target.files) {
      setFormData((prevState) => ({
        ...prevState,
        [e.target.id]: e.target.value,
      }));
    } else {
      setFormData((prevState) => ({
        ...prevState,
        images: e.target.files,
      }));
    }
  }

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);

    if (images === undefined) {
      setLoading(false);
      toast.error("Vous avez sélectionné min un image");
      return;
    }

    if (images.length > 6) {
      setLoading(false);
      toast.error("Maximum 6 images sont autorisées");
    }

    async function storeImage(image) {
      return new Promise((resolve, reject) => {
        const filename = `${auth.currentUser.uid}-${image.name}-${uuidv4()}`;
        const storageRef = ref(storage, filename);
        const uploadTask = uploadBytesResumable(storageRef, image);

        uploadTask.on(
          "state_changed",
          (snapshot) => {
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          },
          (error) => {
            reject(error);
          },
          () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              resolve(downloadURL);
            });
          }
        );
      });
    }

    const imgUrls = await Promise.all(
      [...images].map((image) => storeImage(image))
    ).catch((error) => {
      setLoading(false);
      toast.error("Images non téléchargées");
      return;
    });

    const formDataCopy = {
      ...formData,
      address: "Toulouse",
      imgUrls,
      timestamp: serverTimestamp(),
    };

    delete formDataCopy.images;

    const listingRef = collection(db, "listings");
    const docRef = await addDoc(listingRef, formDataCopy);
    toast.success("Annonce créée");
    setLoading(false);
    setFormData("");
    navigate(`/cars/${docRef.id}`)
  }

  useEffect(() => {
    async function fetchListings() {
      const listingRef = collection(db, "listings");
      const q = query(listingRef, orderBy("timestamp", "desc"));
      const querySnap = await getDocs(q);

      const listings = [];
      querySnap.forEach((doc) => {
        return listings.push({
          id: doc.id,
          data: doc.data(),
        });
      });
      setListings(listings);
    }
    fetchListings();
  }, []);

  if (loading) {
    return <Spinner />;
  }
  return (
    <>
      <section className="flex justify-center items-center">
        <div className="bg-smoke max-w-[780px] p-4">
          <form onSubmit={onSubmit}>
            <div className="flex flex-wrap sm:flex-nowrap sm:space-x-10">
              <div className="w-full mb-6 space-y-2">
                <div className="">
                  <label
                    htmlFor="brand"
                    className="block text-sm font-medium text-candyred"
                  >
                    Marque
                  </label>
                  <input
                    type="text"
                    id="brand"
                    value={brand}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="model"
                    className="block text-sm font-medium text-candyred"
                  >
                    Modèle
                  </label>
                  <input
                    type="text"
                    id="model"
                    value={model}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="mileage"
                    className="block text-sm font-medium text-candyred"
                  >
                    Kilométrage
                  </label>
                  <input
                    type="text"
                    id="mileage"
                    value={mileage}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="price"
                    className="block text-sm font-medium text-candyred"
                  >
                    Prix
                  </label>
                  <input
                    type="text"
                    id="price"
                    value={price}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="gearbox"
                    className="block text-sm font-medium text-candyred"
                  >
                    Type de boite
                  </label>
                  <input
                    type="text"
                    id="gearbox"
                    value={gearbox}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="release"
                    className="block text-sm font-medium text-candyred"
                  >
                    Mise en circulation
                  </label>
                  <input
                    type="text"
                    id="release"
                    value={release}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="power"
                    className="block text-sm font-medium text-candyred"
                  >
                    Puissance
                  </label>
                  <input
                    type="text"
                    id="power"
                    value={power}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="doorNumber"
                    className="block text-sm font-medium text-candyred"
                  >
                    Nombres de portes
                  </label>
                  <input
                    type="number"
                    id="doorNumber"
                    value={doorNumber}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="placesNumber"
                    className="block text-sm font-medium text-candyred"
                  >
                    Nombres de places
                  </label>
                  <input
                    type="number"
                    id="placesNumber"
                    value={placesNumber}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
              </div>
              <div className="w-full mb-6 space-y-2">
                <div>
                  <label
                    htmlFor="transmission"
                    className="block text-sm font-medium text-candyred"
                  >
                    Transmission
                  </label>
                  <input
                    type="text"
                    id="transmission"
                    value={transmission}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="color"
                    className="block text-sm font-medium text-candyred"
                  >
                    Couleur
                  </label>
                  <input
                    type="text"
                    id="color"
                    value={color}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <label
                    htmlFor="option"
                    className="block text-sm font-medium text-candyred"
                  >
                    Option
                  </label>
                  <input
                    type="text"
                    id="option"
                    value={option}
                    onChange={onChange}
                    required
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  />
                </div>
                <div>
                  <p className="text-md text-candyred font-medium">Images</p>
                  <p className="text-candypink text-sm">
                    The first image will be the cover (max 6)
                  </p>
                  <input
                    type="file"
                    id="images"
                    onChange={onChange}
                    accept=".jpg, .png, .jpeg"
                    multiple
                    className="w-full px-3 py-1.5 text-raisin bg-white border border-candyred rounded transition duration-150 ease-in-out focus:bg-white focus:border-candypink"
                  />
                </div>
                <div>
                  <label
                    htmlFor="description"
                    className="block text-sm font-medium text-candyred"
                  >
                    Description
                  </label>
                  <textarea
                    name="description"
                    rows={14}
                    id="description"
                    value={description}
                    onChange={onChange}
                    required
                    placeholder="Description"
                    className="appearance-none block w-full px-3 py-2 rounded-md focus:outline-none text-md"
                  ></textarea>
                </div>
              </div>
            </div>
            <button
              type="submit"
              className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out"
            >
              Rajouter Véhicule
            </button>
          </form>
        </div>
      </section>
      <div className="w-full px-3 mt-6">
        {!loading && listings && (
          <>
            <h2 className="text-h2 text-center font-semibold mb-6">
              Nos annonces
            </h2>
            <ul className="mt-6 mb-6 sm:grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5">
              {listings.map((listing) => (
                <ListingItem 
                key={listing.id}
                id={listing.id}
                listing={listing.data}
                />
              ))}
            </ul>
          </>
        )}
      </div>
    </>
  );
}
