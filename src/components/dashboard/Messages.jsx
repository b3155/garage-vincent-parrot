import React from 'react'
import { useState, useEffect } from 'react'
import { collection, deleteDoc, doc, getDocs, orderBy, query } from "firebase/firestore"
import { db } from "../../utils/firebase-config"
import { toast } from "react-toastify"
import { FaTrash } from "react-icons/fa"

export default function Messages() {

  const [messages, setMessages] = useState(null)

  useEffect(() => {
    async function fetchMessages() {
      try {
        const messageRef = collection(db, "messages")
        const q = query(messageRef, orderBy("timestamp", "desc"))
        const querySnap = await getDocs(q)

        const messages = []
        querySnap.forEach((doc) => {
          return messages.push({
            id: doc.id,
            data: doc.data()
          })
        })
        setMessages(messages)
        console.log(messages);
      } catch (error) {
        toast.error("Pas de message")
      }
    }
    fetchMessages()
  }, [])

  async function onDelete(messageId) {
    if(window.confirm("Ètes-vous sûr de vouloir supprimer la message")) {
      await deleteDoc(doc(db, "messages", messageId))

      const updatedMessages = messages.filter((message) => message.id !== messageId)

      setMessages(updatedMessages)
      toast.success("Message supprimer avec succès")
    }
  }

  return (
    <>
      {messages && messages.length > 0 ? (
        <ul className='p-4'>
          {messages.map((message, i) => (
            <li 
            key={i}
            className="w-full border border-raisin mb-4"
            >
              <h3 className='bg-smoke pl-8 mb-2'>
                {message.data.timestamp.toDate().toLocaleDateString("fr-FR")}
              </h3>
              <p className='text-raisin font-semibold ml-4'>
                From : {message.data.email}
              </p>
              <p className='text-raisin ml-4 mt-2'>
                {message.data.message}
              </p>
              <div className='flex justify-end items-baseline'>
                <a 
                href={`mailto:${message.data.email}`}
                >
                  <button
                  type='button'
                  className='px-4 py-1 bg-candyred text-smoke text-sm font-bold rounded shadow-md uppercase border border-candyred my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out'
                  >
                    Repondre
                  </button>
                </a>
                <FaTrash 
                onClick={() => onDelete(message.id)}
                className='w-[24px] ml-2 text-candypink cursor-pointer hover:text-candyred'
                />
              </div>
            </li>
          ))}
        </ul>
      ) : null}
    </>
  )
}
