import React from 'react'
import DashLayout from '../../utils/DashLayout'
import { auth } from '../../utils/firebase-config'
import { useNavigate } from 'react-router'

export default function Dashboard() {

  const navigate = useNavigate()

  function onLogout() {
    auth.signOut()
    navigate("/")
  }

  return (
    <>
      <div
      className='flex justify-center px-12 py-8 items-center max-w-7xl mx-auto flex-col sm:flex-row sm:justify-between'
      >
        <h3
        className='text-h3 uppercase text-candyred mb-4 sm:mb-0'
        >
          Dashboard
        </h3>
        <button
        onClick={() => onLogout()}
        className='px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out'
        >
          Logout
        </button>
      </div>
      <DashLayout />
    </>
  )
}
