import React from "react";
import { useCookies } from "react-cookie";
import { Link } from "react-router-dom";

export default function CookiesConsent() {
  const [cookies, setCookies] = useCookies(["cookieConsent"]);

  const giveCookieConsent = () => {
    setCookies("cookieConsent", true, { path: "/" });
  };

  return (
    <div className="sticky bottom-0 z-50 w-full text-md sm:flex justify-center items-center bg-raisin text-smoke font-semibold px-4 py-10 border-t-4 border-candyred">
      <p>
        Nous utilisons des cookies pour améliorer votre expérience
        utilisateur.En utilisant notre site Web, vous acceptez notre utilisation des cookies.{""}
        <Link to="/privacy-policy">
          <span className="text-candypink hover:text-candyred ml-2">
            Savoir plus
          </span>
        </Link>
      </p>
      <button 
      onClick={giveCookieConsent}
      className="px-4 py-1 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
        Accepter
      </button>
    </div>
  );
}
