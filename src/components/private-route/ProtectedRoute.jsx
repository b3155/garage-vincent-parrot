import React from 'react'
import useAuthStatus from "../../hooks/useAuthStatus"
import { Outlet, Navigate } from 'react-router'
import Spinner from "../Spinner"

export default function ProtectedRoute() {

  const {loggedIn, checkingStatus} = useAuthStatus()

  if(checkingStatus) {
    return <Spinner />
  }

  return loggedIn ? <Outlet /> : <Navigate to="/sign-in" />
}
