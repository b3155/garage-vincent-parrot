import React from "react";
import logo from "../assets/logo.png";
import hamburger from "../assets/hamburger.svg";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../utils/firebase-config";

export default function Header() {

  const [showMenu, setShowMenu] = useState(false)
  const location = useLocation()
  const navigate = useNavigate()
  const [pageState, setPageState] = useState("Login")

  function pathRoute(route) {
    if(route === location.pathname) {
      return true
    }
  }

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if(user) {
        setPageState("Dashboard")
      } else {
        setPageState("Login")
      }
    })
  }, [])

  return (
    <header className="sticky top-0 w-full z-50 shadow-md bg-raisin sm:mb-6">
      <div className="flex justify-between items-center px-4 py-2 max-w-7xl mx-auto">
        <Link
        to="/"
        className="w-[100px] cursor-pointer"
        >
          <img className=" w-full h-full object-center object-cover" src={logo} alt="logo" />
        </Link>
        <nav>
          <ul 
          className={`${showMenu ? "flex" : "hidden"} absolute top-full flex flex-col items-center w-full bg-raisin left-0 pb-5 text-sm font-semibold text-smoke font-barlow space-y-4 sm:space-y-0 sm:relative sm:flex sm:flex-row sm:pb-0 sm:space-x-4`}
          >
            <li 
            onClick={() => {navigate("/"), setShowMenu(false)}}
            className={`cursor-pointer ${pathRoute("/") && "border-b-[3px] border-b-candyred"}`}>Accueil</li>
            <li 
            onClick={() => {navigate("/cars"), setShowMenu(false)}}
            className={`cursor-pointer ${pathRoute("/cars") && "border-b-[3px] border-b-candyred"}`}>Nos Véhicule</li>
            <li 
            onClick={() => {navigate("/contact"), setShowMenu(false)}}
            className={`cursor-pointer ${pathRoute("/contact") && "border-b-[3px] border-b-candyred"}`}>Contact</li>
            <li 
            onClick={() => {
              if(pageState === "Login") {
                navigate("/sign-in")
              }
              if(pageState === "Dashboard") {
                navigate("/dashboard")
              }
              setShowMenu(false)}}
            className={`cursor-pointer ${pathRoute("/sign-in") && "border-b-[3px] border-b-candyred"}`}>
              {pageState}
            </li>
          </ul>
        </nav>
        <button
        onClick={() => setShowMenu(!showMenu)} 
        className="ml-auto flex sm:hidden"
        >
          <img className="w-6" src={hamburger} alt={showMenu ? "Cacher menu" : "Montrer menu"} />
        </button>
      </div>
    </header>
  );
}
