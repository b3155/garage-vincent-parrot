import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import helpYou from "../resources/help-you.jpg"
import { FaPhone } from "react-icons/fa6";
import { useState } from "react";


export default function NavServices() {
  const [showMenu, setShowMenu] = useState(false)
  const location = useLocation();

  function pathRoute(route) {
    if (route === location.pathname) {
      return true;
    }
  }

  return (
    <div>
      <button 
      onClick={() => setShowMenu(!showMenu)}
      className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 sm:mx-0">
      Tous les Services
      </button>
      <nav className={`${showMenu ? "flex" : "hidden"} relative sm:min-w-[180px] p-2 sm:w-full sm:translate-x-0 transition duration-200 ease-in-out sm:flex`}>
        <ul className="flex flex-col">
          <Link to="/maintenance">
            <li
              className={`mb-2 py-3 text-md font-semibold border-b border-raisin ${
                pathRoute("/maintenance") ? "text-alizarin" : "text-raisin"
              }`}
            >
              Entretien Régulier
            </li>
          </Link>
          <Link to="/diagnostic">
            <li
              className={`mb-2 py-3 text-md font-semibold border-b border-raisin ${
                pathRoute("/diagnostic") ? "text-alizarin" : "text-raisin"
              }`}
            >
              Mécanique de la voiture
            </li>
          </Link>
          <Link to="/car-body">
            <li
              className={`mb-2 py-3 text-md font-semibold border-b border-raisin ${
                pathRoute("/car-body") ? "text-alizarin" : "text-raisin"
              }`}
            >
              Carrosserie
            </li>
          </Link>
          <Link to="/used-cars">
            <li
              className={`mb-2 py-3 text-md font-semibold border-b border-raisin ${
                pathRoute("/used-cars") ? "text-alizarin" : "text-raisin"
              }`}
            >
              Véhicules d'occasion
            </li>
          </Link>
        </ul>
      </nav>
      <div 
      className="relative w-full h-[450px] mt-36"
      style={{
        background: `url(${helpYou}) center / cover no-repeat`,
      }}
      >
        <div className="w-full h-[80px] absolute bottom-0 bg-raisin flex justify-center items-center">
          <div className="rounded-full bg-candyred w-10 h-10 flex justify-center items-center">
            <FaPhone className=" text-white" />
          </div>
          <div className="text-md text-smoke font-bold text-center ml-2">
            <p>Besoin d'aide</p>
            <p>++33 865 234 234</p>
          </div>
        </div>
      </div>
    </div>
  );
}
