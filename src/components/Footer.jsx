import React from "react";
import logo from "../assets/logo.png";
import { FaRegEnvelopeOpen } from "react-icons/fa";
import { FiPhone } from "react-icons/fi";
import { Link } from "react-router-dom";
import WorkTime from "./dashboard/WorkTime";

export default function Footer() {
  return (
    <footer className="bottom-0 bg-raisin w-full">
      <div className="max-w-7xl mx-auto flex flex-wrap justify-center p-4">
        <div className="w-1/4 min-w-[220px] flex flex-col items-center">
          <Link to="/">
            <img className="max-w-[150px]" src={logo} alt="logo" />
          </Link>
          <div className="flex justify-center items-baseline">
            <FiPhone className="text-candyred w-[20px]" />
            <p className="text-smoke ml-2">Phone</p>
          </div>
          <p className="text-smoke mb-2">++33 865 234 234</p>
          <div className="flex justify-center items-baseline">
            <FaRegEnvelopeOpen className="text-candyred w-[20px]" />
            <p className="text-smoke ml-2">Email</p>
          </div>
          <p className="text-smoke mb-2">parrotvincent@gmail.com</p>
        </div>
        <div className="w-1/4 min-w-[220px] flex flex-col items-start p-4 text-smoke text-sm">
          <h5 className="text-candyred uppercase font-semibold mt-8 mb-4">
            Nos services
          </h5>
          <Link to="/maintenance">
            Entretien régulier
          </Link>
          <Link to="/diagnostic">
            Mcanique de la voiture
          </Link>
          <Link to="/car-body">
            Carrosserie
          </Link>
          <Link to="/used-cars">
            Vente de véhicules d'occasion
          </Link>
          <Link to="/privacy-policy">
            Politique de confidentialité
          </Link>
          <Link to="/legal-notice">
            Mentions légales
          </Link>
        </div>
        <div className="w-1/4 min-w-[220px] flex flex-col items-start p-4">
          <h5 className="text-candyred uppercase font-semibold mt-8 mb-4">
            Horaires
          </h5>
          <WorkTime />
        </div>
        <div className="w-1/4 min-w-[220px] flex flex-col items-center p-4">
          <h5 className="text-candyred uppercase font-semibold mt-8 mb-4">
            Adresse
          </h5>
          <p className="text-smoke text-lg">Toulouse</p>
          <Link to="/contact">
            <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-6 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Contact
            </button>
          </Link>
        </div>
      </div>
      <div className="w-full bg-black text-smoke text-sm font-medium text-center p-1">
        <p>
          Copyright 2023 All Rights Reserved By{" "}
          <span className="text-candyred font-semibold block sm:inline-block">
            Garage Vincent Parrot
          </span>
        </p>
      </div>
    </footer>
  );
}
