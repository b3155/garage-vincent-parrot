import React from 'react'
import spinner from "../assets/spinner.svg"

export default function Spinner() {
  return (
    <div
    className='bg-black bg-opacity-50 flex items-center justify-center fixed top-0 right-0 bottom-0 left-0 z-50'
    >
      <div>
        <img src={spinner} alt="Loading ..." className='w-8' />
      </div>
    </div>
  )
}
