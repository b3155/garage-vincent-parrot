import React from 'react'
import { Link } from 'react-router-dom'
import Moment from "react-moment"

export default function ListingItem({listing, id}) {
  return (
    <li
    className='relative bg-raisin flex flex-col justify-between items-center m-[10px] mb-10 shadow-md hover:shadow-xl overflow-hidden transition duration-150'
    >
      <Link to={`/cars/${id}`} className="contents">
        <img 
        src={listing.imgUrls[0]} 
        alt="car listing"
        loading='lazy'
        className='h-[200px] w-full object-cover hover:scale-105 transition-scale duration-200 ease-in' 
        />
        <Moment
        fromNow
        className='absolute top-2 left-2 bg-[#3377cc] text-white uppercase text-xs font-semibold rounded-md px-2 py-1 shadow-lg'
        >
          {listing.timestamp?.toDate()}
        </Moment>
        <div className='w-full p-[10px] text-smoke'>
          <div className='flex items-center space-x-1'>
            <p className='text-sm font-bold truncate'>
              {listing.brand}
            </p>
            <p className='text-sm font-bold'>{listing.model}</p>
          </div>
          <div className='flex items-center space-x-1'>
            <p className='text-xs font-bold'>
              Année : {listing.release}
            </p>
          </div>
          <div className='flex items-center space-x-1'>
            <p className='text-xs font-bold'>
              {listing.mileage}km
            </p>
          </div>
          <div className='flex items-center space-x-1'>
            <p className='text-xs font-bold'>
              {listing.price} euros
            </p>
          </div>
          <button
          className='block ml-auto bg-candyred px-5 rounded text-smoke hover:bg-candypink transition duration-150 ease-in-out'
          >
            Detail
          </button>
        </div>
      </Link>
    </li>
  )
}
