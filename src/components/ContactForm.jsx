import React from 'react'
import { addDoc, collection, serverTimestamp } from 'firebase/firestore'
import { useState } from 'react'
import { db } from '../utils/firebase-config'
import Spinner from './Spinner'
import { toast } from "react-toastify"

export default function ContactForm() {

  const [loading, setLoading] = useState(false)
  const [formData, setFormData] = useState({
    name: "",
    firstname: "",
    email: "",
    phone: "",
    message: "",
    timestamp: serverTimestamp()
  })
  const{
    name,
    firstname,
    email,
    phone,
    message,
    timestamp
  } = formData

  function onChange(e) {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value
    }))
  }

  async function onSubmit(e) {
    e.preventDefault()
    setLoading(true)

    try {
      const messageRef = collection(db, "messages")
      await addDoc(messageRef, formData)
      setLoading(false)
      setFormData("")
      toast.success("Votre message est bien envoye")
    } catch (error) {
      toast.error("Votre message ne pas envoye")
    }
  }

  if(loading) {
    return <Spinner />
  }

  return (
    <>
      <div>
        <form 
        onSubmit={onSubmit}
        className='p-8 bg-raisin rounded-md'>
          <h3 className='text-h3 text-candyred font-semibold'>
            Envoyer message
          </h3>
          <div>
            <div className='mt-6'>
              <label 
              htmlFor="name"
              className='block text-sm text-candyred font-medium'
              >
                Votre Nom
              </label>
              <input 
              type="text" 
              id='name'
              required
              value={name}
              onChange={onChange}
              className='appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md'
              />
            </div>
            <div>
              <label 
              htmlFor="firstname"
              className='block text-sm text-candyred font-medium'
              >
                Votre Prenom
              </label>
              <input 
              type="text" 
              id='firstname'
              required
              value={firstname}
              onChange={onChange}
              className='appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md'
              />
            </div>
            <div>
              <label 
              htmlFor="email"
              className='block text-sm text-candyred font-medium'
              >
                Votre Email
              </label>
              <input 
              type="text" 
              id='email'
              required
              value={email}
              onChange={onChange}
              className='appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md'
              />
            </div>
            <div>
              <label 
              htmlFor="phone"
              className='block text-sm text-candyred font-medium'
              >
                Votre numéro de téléphone
              </label>
              <input 
              type="text" 
              id='phone'
              required
              value={phone}
              onChange={onChange}
              className='appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md'
              />
            </div>
            <div>
              <textarea
              name="message" 
              id="message" 
              rows={4}
              required
              placeholder="Votre message ..."
              value={message}
              onChange={onChange}
              className='appearance-none block w-full px-3 py-2 rounded-md focus:outline-none sm:text-md mt-4'
              ></textarea>
            </div>
          </div>
          <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Envoyer message
          </button>
        </form>
      </div>
    </>
  )
}
