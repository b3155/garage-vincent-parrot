import React from "react";
import entretien from "../../resources/entretien.jpg";
import diagnostic from "../../resources/diagnostique.jpg";
import carrosserie from "../../resources/carrosserie.jpg";
import occasion from "../../resources/occasion.jpg";
import { Link } from "react-router-dom";

export default function ServicesSection() {
  return (
    <section>
      <h3 className="relative text-h3 text-candyred font-semibold text-center uppercase mt-6">
        <span className="bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-2 md:top-4 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
        </span>
        Nos meilleurs services
        <span className="bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-2 md:top-4 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
        </span>
      </h3>
      <h2 className="text-h2 font-semibold text-center my-6">
        Opportunité de Service de Qualité
      </h2>
      <div className="w-full px-10 py-6 justify-center items-center grid gap-6 md:grid-cols-2 xl:grid-cols-4">
        <div className="flex justify-center items-center h-[450px]">
          <div className="flex flex-col items-center max-w-[300px] bg-raisin h-full">
            <div className="flex h-[250px]">
              <img src={entretien} alt="img maintenance" />
            </div>
            <div>
              <p className="text-xl text-smoke font-semibold mt-6 mb-8">
                Entretien regulier
              </p>
              <Link to="/maintenance">
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Lire plus
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center h-[450px]">
          <div className="flex flex-col items-center max-w-[300px] bg-raisin h-full">
            <div className="flex h-[250px]">
              <img src={diagnostic} alt="img diagnostic" />
            </div>
            <div>
              <p className="text-xl text-smoke font-semibold mt-6 mb-8">
                Mécanique de la voiture
              </p>
              <Link to="/diagnostic">
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Lire plus
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center h-[450px]">
          <div className="flex flex-col items-center max-w-[300px] bg-raisin h-full">
            <div className="flex h-[250px]">
              <img src={carrosserie} alt="img car body" />
            </div>
            <div>
              <p className="text-xl text-smoke font-semibold mt-6 mb-8">
                Carrosserie
              </p>
              <Link to="/car-body">
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Lire plus
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center h-[450px]">
          <div className="flex flex-col items-center max-w-[300px] bg-raisin h-full">
            <div className="flex h-[250px]">
              <img src={occasion} alt="img used cars" />
            </div>
            <div>
              <p className="text-xl text-smoke font-semibold mt-6 mb-8">
                Véhicules d'occasion
              </p>
              <Link to="/used-cars">
                <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
                  Lire plus
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
