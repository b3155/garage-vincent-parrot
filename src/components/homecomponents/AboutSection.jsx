import React from "react";
import solution from "../../resources/solution.jpg";
import { Link } from "react-router-dom";
import { FaCheckCircle } from "react-icons/fa"

export default function AboutSection() {
  return (
    <section className="my-20 pb-10">
      <div className="flex flex-col m-10 space-y-8 md:flex-row md:space-y-0 md:space-x-8">
        <div
          className="relative h-[450px] md:w-[50%]"
          style={{
            background: `url(${solution}) center / cover no-repeat`,
          }}
        >
          <div className="absolute bottom-4 right-4 bg-alizarin w-[150px] h-[180px] flex flex-col justify-center items-center p-4">
            <h4 className="text-3xl text-smoke font-bold">15+</h4>
            <p className="text-sm text-smoke font-semibold">
              Années de travail<br></br>Et d'expériences
            </p>
          </div>
        </div>
        <div className="relative bg-candypink h-[450px] p-4 md:w-[50%]">
          <div>
            <h4 className="text-xl uppercase font-bold text-alizarin ml-10 sm:mb-4">
              <span className="bg-candyred w-6 h-1 absolute transform -translate-x-7 top-2 mt-4 inline-block">
                <span className="bg-candyred w-4 h-1 absolute transform translate-x-2 top-2 inline-block"></span>
              </span>
              A propos
            </h4>
            <h5 className="text-raisin text-xl font-bold sm:mb-4">
              Nous sommes la solution pour<br></br>Votre voiture
            </h5>
            <p className="text-sm text-raisin font-semibold sm:mb-4">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut illum
              magnam consequuntur dicta impedit? Doloribus consequatur illum
              error commodi tenetur!
            </p>
            <div className="relative">
              <div>
                <div className="text-sm border-b border-raisin w-1/2 sm:space-y-2 sm:pb-4">
                  <p>
                    <FaCheckCircle className="inline-block mr-2 text-[#d92332] font-bold" />
                    Technicien Expert
                  </p>
                  <p>
                    <FaCheckCircle className="inline-block mr-2 text-[#d92332] font-bold" />
                    Diagnostic et réparation précis
                  </p>
                  <p>
                    <FaCheckCircle className="inline-block mr-2 text-[#d92332] font-bold" />
                    Réalisation de qualité
                  </p>
                </div>
                <div className="text-md text-raisin font-semibold sm:space-y-2">
                  <p>Appelez nous :</p>
                  <p className="text-candyred">++33 865 234 234</p>
                </div>
              </div>
              <div className="hidden sm:block absolute bottom-0 right-0 bg-smoke p-2 md:flex flex-col justify-center items-center sm:-bottom-6 sm:w-[130px] sm:h-[180px]">
                <div className="flex flex-col justify-center items-center border-b border-raisin pb-2 text-sm text-raisin">
                  <p className="text-md font-bold">100%</p>
                  <p>Taux de réussite</p>
                </div>
                <div className="flex flex-col justify-center items-center pt-2 text-sm text-raisin">
                  <p className="text-md font-bold">2000+</p>
                  <p>Clients satisfaits</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Link to="/about">
        <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto  hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
          À Propos de Nous
        </button>
      </Link>
    </section>
  );
}
