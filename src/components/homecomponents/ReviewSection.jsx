import React from "react";
import reviews1 from "../../resources/reviews.jpg";
import reviews2 from "../../resources/reviews2.jpg";
import { FaCalendarAlt, FaCar, FaRegHandshake, FaGlobe } from "react-icons/fa";
import { BsHandThumbsUp } from "react-icons/bs";
import { LiaAwardSolid } from "react-icons/lia";
import { FaPeopleGroup } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { collection, getDocs, limit, orderBy, query } from "firebase/firestore";
import { db } from "../../utils/firebase-config"
import { toast } from "react-toastify";
import ReactStars from "react-rating-stars-component";
import { comment } from "postcss";

export default function ReviewSection() {
  const [reviews, setReviews] = useState(null);

  useEffect(() => {
    async function fetchComments() {
      try {
        const commentsRef = collection(db, "customersReviews");
        const q = query(
          commentsRef,
          orderBy("data.timestamp", "desc"),
          limit(1)
        );
        const querySnap = await getDocs(q);

        const reviews = [];
        querySnap.forEach((doc) => {
          return reviews.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setReviews(reviews);
      } catch (error) {
        toast.error("Impossible de récupérer le commentaire");
      }
    }
    fetchComments();
  }, []);

  return (
    <section>
      <div
        className="relative w-full h-[550px]"
        style={{
          background: `url(${reviews1}) center / cover no-repeat`,
        }}
      >
        <div className="flex flex-col w-full h-full justify-center items-center space-y-2 px-36 py-4 bg-black opacity-75 md:flex-row md:justify-between">
          <div className="flex w-1/2 h-full justify-center space-x-8 md:justify-around">
            <div className="flex flex-col items-center md:justify-center">
              <div className="flex justify-center items-center rounded-full bg-alizarin w-[80px] h-[80px]">
                <FaCalendarAlt className="text-white w-6 h-6" />
                <FaCar className="text-white w-6 h-6" />
              </div>
              <p className="text-white text-md font-bold text-center">1000+</p>
              <p className="text-white text-md font-bold text-center">
                Solution de<br></br>réparation
              </p>
            </div>
            <div className="flex flex-col items-center md:justify-center">
              <div className="flex justify-center items-center rounded-full bg-alizarin w-[80px] h-[80px]">
                <FaGlobe className="text-white w-6 h-6" />
                <BsHandThumbsUp className="text-white w-6 h-6" />
              </div>
              <p className="text-white text-md font-bold text-center">15+</p>
              <p className="text-white text-md font-bold text-center">
                Années<br></br> d'expèriance
              </p>
            </div>
          </div>
          <div className="flex w-1/2 h-full justify-center space-x-8 md:justify-around">
            <div className="flex flex-col items-center md:justify-center">
              <div className="flex justify-center items-center rounded-full bg-alizarin w-[80px] h-[80px]">
                <LiaAwardSolid className="text-white w-10 h-10" />
              </div>
              <p className="text-white text-md font-bold text-center">10+</p>
              <p className="text-white text-md font-bold text-center">
                Recompenses<br></br>gagnantes
              </p>
            </div>
            <div className="flex flex-col items-center md:justify-center">
              <div className="flex flex-col justify-center items-center rounded-full bg-alizarin w-[80px] h-[80px]">
                <FaPeopleGroup className="text-white w-6 h-6" />
                <FaRegHandshake className="text-white w-6 h-6" />
              </div>
              <p className="text-white text-md font-bold text-center">15+</p>
              <p className="text-white text-md font-bold text-center">
                Mombres d'équipe<br></br>d'experts
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="relative py-10 sm:px-20 md:-top-[150px]">
        <div className="bg-white p-6 md:flex md:space-x-4">
          <div className="md:w-1/2">
            <h4 className="relative ml-6 text-xl uppercase text-candyred font-semibold mb-2">
              <span className="bg-candyred w-6 h-1 absolute transform -translate-x-7 top-2 inline-block">
                <span className="bg-candyred w-4 h-1 absolute transform translate-x-2 top-2 inline-block"></span>
              </span>
              Temoignages
            </h4>
            <h5 className="text-xl font-bold mb-4">
              Ce que Nos Clients disent<br></br>De Nous
            </h5>
            {reviews && (
              <>
                {reviews.map((review, i) => (
                  <div key={i} className="bg-smoke p-2">
                    <p className="text-md text-semibold">
                      {review.data.data.comment}
                    </p>
                    <div className="sm:flex justify-between py-6 px-4">
                      <p className="text-md font-bold">{review.data.data.name} {review.data.data.firstname}</p>
                      <ReactStars 
                      count={5}
                      size={24}
                      value={review.data.data.rating}
                      edit={false}
                      />
                    </div>
                  </div>
                ))}
              </>
            )}
          </div>
          <div
            className="h-[300px] md:w-1/2"
            style={{
              background: `url(${reviews2}) center / cover no-repeat`,
            }}
          ></div>
        </div>
        <div className="flex flex-col justify-center items-center mt-6 md:space-x-8 md:flex-row">
          <Link to="/customers-reviews">
            <button className="min-w-[250px] px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Voir tous les commentaires
            </button>
          </Link>
          <Link to="/reviews">
            <button className="min-w-[250px] px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Donnez Votre Avis
            </button>
          </Link>
        </div>
      </div>
      <div></div>
    </section>
  );
}
