import React from "react";
import experiences from "../../resources/experiences.jpg";
import about from "../../resources/about1.jpg";
import { FiPhone } from "react-icons/fi";
import { FaTrophy } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function Experience() {
  return (
    <section>
      <h3 className="relative text-h3 text-raisin font-semibold text-center uppercase mt-6">
        <span className="bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-2 md:top-4 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
        </span>
        À propos de nous
        <span className="bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-2 md:top-4 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
        </span>
      </h3>
      <div className="flex flex-col md:flex-row px-10 my-24">
        <div
          className="w-full min-h-[550px] overflow-hidden md:w-1/2"
          style={{
            background: `url(${experiences}) center / cover no-repeat`,
          }}
        ></div>
        <div className="w-full md:w-1/2 p-6">
          <div>
            <h4 className="text-xl text-raisin font-semibold mb-2">
              Faites l'expérience d'une Réparation automobile<br></br>Supérieure
              avec nous
            </h4>
            <p className="text-md text-raisin font-medium">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quaerat
              voluptates, quibusdam, itaque deserunt consectetur distinctio
              voluptas illum vel saepe delectus dolore odio voluptate officiis
              accusantium ratione sint excepturi iusto tempora?Lorem ipsum dolor
              sit amet consectetur adipisicing elit. Quaerat natus magnam
              eveniet fugiat illum dolorum ipsum accusamus sit consequatur
              exercitationem.
            </p>
          </div>
          <div className="flex justify-between">
            <div className="sm:max-w-[300px]">
              <div className="border-b border-raisin">
                <div className="flex justify-start items-baseline my-2">
                  <FaTrophy className="text-candyred mr-2 w-6" />
                  <h5 className="text-md text-raisin font-semibold">
                    Entreprise Primée
                  </h5>
                </div>
                <p className="text-md text-raisin ml-4">
                  Les entreprises primées excellent souvent dans des domaines
                  tels que la mécanique
                </p>
              </div>
              <div>
                <div className="flex justify-start items-baseline my-2">
                  <FiPhone className="text-candyred mr-2 w-6" />
                  <h5 className="text-md text-raisin font-semibold">
                    Soutien Amical
                  </h5>
                </div>
                <p className="text-md text-raisin ml-4">
                  Les entreprises primées excellent souvent dans des domaines
                  tels que la relation client
                </p>
              </div>
            </div>
            <div
              className="hidden w-[150px] h-[180px] relative -bottom-10 sm:flex flex-col justify-center items-center"
              style={{
                background: `url(${about}) center / cover no-repeat`,
              }}
            >
              <p className="text-2xl text-candyred font-bold">15+</p>
              <p className="text-md text-alizarin text-center font-bold">
                Années<br></br>d'experiences
              </p>
            </div>
          </div>
          <Link to="/contact">
            <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Contactez Nous
            </button>
          </Link>
        </div>
      </div>
    </section>
  );
}
