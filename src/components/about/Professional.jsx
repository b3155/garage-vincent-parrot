import React from "react";
import bgPro from "../../resources/bg-pro.jpg";
import { Link } from "react-router-dom";

export default function Professional() {
  return (
    <section>
      <div
        className="w-full h-[550px] mb-10"
        style={{
          background: `url(${bgPro}) center / cover no-repeat`,
        }}
      >
        <div className="w-full h-full bg-black opacity-75 flex flex-col justify-center items-center p-4">
          <h3 className="relative text-md sm:text-xl text-candyred font-bold text-center uppercase mt-6">
            <span className="hidden bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-2 sm:inline-block">
              <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
            </span>
            Faites réparer par un professionnel
            <span className="hidden bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-2 sm:inline-block">
              <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
            </span>
          </h3>
          <p className="text-white text-xl font-bold text-center">
            Redonnez à votre voiture son aspect neuf<br></br>Grâce a nos experts
            en réparation
          </p>
          <Link to="/contact">
            <button className="px-7 py-2 bg-candyred text-smoke font-semibold text-md rounded-md border border-candyred shadow-md block mx-auto my-4 hover:bg-smoke hover:shadow-lg hover:text-raisin transition duration-150 ease-in-out">
              Contactez Nous
            </button>
          </Link>
        </div>
      </div>
    </section>
  );
}
