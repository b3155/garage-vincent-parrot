import React from "react";
import mecanitien from "../../resources/mecanitien.png"
import carrossier from "../../resources/carrossier.png"
import expert from "../../resources/expert.png"

export default function Experts() {
  return (
    <section>
      <h4 className="relative text-xl text-candyred font-bold text-center uppercase mt-6">
        <span className="bg-[#d92332] w-6 h-1 absolute transform -translate-x-7 top-2 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-1 top-2 inline-block"></span>
        </span>
        Technicien expert
        <span className="bg-[#d92332] w-6 h-1 absolute transform translate-x-1 top-2 inline-block">
          <span className="bg-[#d92332] w-4 h-1 absolute transform -translate-x-3 top-2 inline-block"></span>
        </span>
      </h4>
      <h3 className="text-h3 text-raisin text-center font-semibold">
        Rencontrez nos Experts
      </h3>
      <div className="mt-10 flex flex-wrap justify-center items-center py-10 xl:justify-between">
        <div
          className="relative h-[550px] min-w-[300px] flex justify-center items-end m-4"
          style={{
            background: `url(${mecanitien}) center / cover no-repeat`,
          }}
        >
          <div className="bg-smoke w-[200px] py-2 text-center border-b border-raisin">
            <h5 className="text-xl text-raisin font-semibold">Joseph Carter</h5>
            <p className="text-md text-candyred font-bold">Moteur Expert</p>
          </div>
        </div>
        <div
          className="relative h-[550px] min-w-[300px] flex justify-center items-end m-4"
          style={{
            background: `url(${carrossier}) center / cover no-repeat`,
          }}
        >
          <div className="bg-smoke w-[200px] py-2 text-center border-b border-raisin">
            <h5 className="text-xl text-raisin font-semibold">Michael Daniel</h5>
            <p className="text-md text-candyred font-bold">Expert en Carrosserie</p>
          </div>
        </div>
        <div
          className="relative h-[550px] min-w-[300px] flex justify-center items-end m-4"
          style={{
            background: `url(${expert}) center / cover no-repeat`,
          }}
        >
          <div className="bg-smoke w-[200px] py-2 text-center border-b border-raisin">
            <h5 className="text-xl text-raisin font-semibold">Aiden Samuel</h5>
            <p className="text-md text-candyred font-bold">Moteur Expert</p>
          </div>
        </div>
      </div>
    </section>
  );
}
