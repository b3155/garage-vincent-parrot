import { Routes, Route } from "react-router";
import Home from "./pages/Home";
import Cars from "./pages/Cars";
import Filter from "./pages/Filter";
import Contact from "./pages/Contact";
import SignIn from "./pages/SignIn";
import ForgotPassword from "./pages/ForgotPassword";
import Header from "./components/Header";
import Dashboard from "./components/dashboard/Dashboard";
import NewCar from "./components/dashboard/NewCar";
import Testimonial from "./components/dashboard/Testimonial";
import Messages from "./components/dashboard/Messages";
import ChangeWorkTime from "./components/dashboard/ChangeWorkTime";
import AllCars from "./components/dashboard/AllCars";
import Employee from "./components/dashboard/Employee";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ProtectedRoute from "./components/private-route/ProtectedRoute";
import Listing from "./pages/Listing";
import EditListing from "./components/dashboard/EditListing"
import Maintenance from "./pages/Maintenance";
import Diagnostic from "./pages/Diagnostic";
import CarBody from "./pages/CarBody"
import UsedCars from "./pages/UsedCars"
import About from "./pages/About"
import CustomersReviews from "./pages/CustomersReviews";
import Reviews from "./pages/Reviews";
import PrivacyPolicy from "./pages/PrivacyPolicy";
import LegalNotice from "./pages/LegalNotice";

import CookiesConsent from "./components/CookiesConsent"
import { useCookies } from "react-cookie"

import { useLocation } from "react-router-dom"
import { useEffect } from "react"

function App() {

  const [cookies] = useCookies(["cookieConsent"])
  const pathname = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/cars" element={<Cars />} />
        <Route path="/filter" element={<Filter />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/sign-in" element={<SignIn />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/maintenance" element={<Maintenance />} />
        <Route path="/diagnostic" element={<Diagnostic />} />
        <Route path="/car-body" element={<CarBody />} />
        <Route path="/used-cars" element={<UsedCars />} />
        <Route path="/about" element={<About />} />
        <Route path="/customers-reviews" element={<CustomersReviews />} />
        <Route path="/reviews" element={<Reviews />} />
        <Route path="privacy-policy" element={<PrivacyPolicy />} />
        <Route path="/legal-notice" element={<LegalNotice />} />
        <Route path="/edit-listing" element={<ProtectedRoute />} >
          <Route path="/edit-listing/:listingId" element={<EditListing />} />
        </Route>
        <Route path="/dashboard" element={<ProtectedRoute />}>
          <Route path="/dashboard" element={<Dashboard />}>
            <Route path="/dashboard/new-car" element={<NewCar />} />
            <Route path="/dashboard/testimonial" element={<Testimonial />} />
            <Route path="/dashboard/messages" element={<Messages />} />
            <Route path="/dashboard/change-work-time" element={<ChangeWorkTime />} />
            <Route path="/dashboard/all-cars" element={<AllCars />} />
            <Route path="/dashboard/employee" element={<Employee />} />
          </Route>
        </Route>
        <Route path="/cars/:listingId" element={<Listing />} />
      </Routes>
      <ToastContainer
        position="bottom-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />

      {!cookies.cookieConsent && <CookiesConsent />}
    </>
  );
}

export default App;
