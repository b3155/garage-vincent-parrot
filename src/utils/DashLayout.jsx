import React from "react";
import { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { auth } from "../utils/firebase-config";

export default function DashLayout() {
  const [showMenu, setShowMenu] = useState(false)
  const location = useLocation();

  const isAdmin = import.meta.env.VITE_IS_ADMIN
  const admin = auth.currentUser.email === isAdmin

  function pathRoute(route) {
    if (route === location.pathname) {
      return true;
    }
  }

  return (
    <div className="max-w-7xl min-h-screen mx-auto bg-white">
      <div className="flex justify-between m-4">
        <div className="relative">
          <button 
          onClick={() => setShowMenu(!showMenu)}
          className="absolute mt-2 ml-2 px-2 bg-candyred rounded text-smoke font-bold sm:hidden">
            Menu
          </button>

          <nav className={`${showMenu ? "flex" : "hidden"} absolute z-10 sm:relative bg-raisin text-smoke sm:bg-transparent transform translate-x-[0] sm:min-h-screen min-w-[230px] border-0 border-r-2 border-candyred p-2 mt-12 sm:w-1/4 sm:translate-x-0 sm:flex transition duration-200 ease-in-out`}>
            <ul className="flex flex-col">
              <div className="border-b-2 border-candyred">
                <Link to="/dashboard/new-car">
                  <li
                    onClick={() => setShowMenu(false)}
                    className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                      pathRoute("/dashboard/new-car") && "text-alizarin"
                    }`}
                  >
                    Rajouter une nouvelle voiture
                  </li>
                </Link>
                <Link to="/dashboard/testimonial">
                  <li
                    onClick={() => setShowMenu(false)}
                    className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                      pathRoute("/dashboard/testimonial") && "text-alizarin"
                    }`}
                  >
                    Témoignages
                  </li>
                </Link>
              </div>
              {admin && (
                <>
                  <Link to="/dashboard/messages">
                    <li
                      onClick={() => setShowMenu(false)}
                      className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                        pathRoute("/dashboard/messages") && "text-alizarin"
                      }`}
                    >
                      Messages
                    </li>
                  </Link>
                  <Link to="/dashboard/change-work-time">
                    <li
                      onClick={() => setShowMenu(false)}
                      className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                        pathRoute("/dashboard/change-work-time") && "text-alizarin"
                      }`}
                    >
                      Horaires
                    </li>
                  </Link>
                  <Link to="/dashboard/all-cars">
                    <li
                      onClick={() => setShowMenu(false)}
                      className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                        pathRoute("/dashboard/all-cars") && "text-alizarin"
                      }`}
                    >
                      Toutes les voitures
                    </li>
                  </Link>
                  <Link to="/dashboard/employee">
                    <li
                      onClick={() => setShowMenu(false)}
                      className={`mb-2 py-3 text-md font-semibold sm:text-raisin ${
                        pathRoute("/dashboard/employee") && "text-alizarin"
                      }`}
                    >
                      Salariés
                    </li>
                  </Link>
                </>
              )}
            </ul>
          </nav>
        </div>
        <main role="main" className="w-full py-4">
          <div>
            <div className="flex justify-end">
              <p className="m-2 text-candyred font-semibold sm:text-xl">
                Bonjour et Bienvenue
              </p>
            </div>
          </div>
          <Outlet />
        </main>
      </div>
    </div>
  );
}
